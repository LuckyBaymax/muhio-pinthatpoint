//
//  AppConstant.swift
//  PinThatPoint
//
//  Created by Apple on 10/1/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import Foundation
import UIKit

//API
let kAPILogin = "https://www.arrivy.com/api/users/login"
let kAPTrack = "https://www.arrivy.com/api/entities"
let kAPISendLocation = "https://www.arrivy.com/api/entities/report"
let kAPILogout = "https://www.arrivy.com/api/users/logout"
let kAPIRegister = "https://www.arrivy.com/api/users/register"
let kAPIProfile = "https://www.arrivy.com/api/users/profile"
let kAPITaks = "https://www.arrivy.com/api/tasks"
let kAPIForgetPass = "https://www.arrivy.com/api/users/resetpassword"
let kAPIUpdateStauts = "https://www.arrivy.com/api/tasks/"
let kAPIEstimate = "https://www.arrivy.com/api/tasks/"
let kAPIEmergency = "https://www.arrivy.com/api/emergency"

let kURLHelp = "https://www.arrivy.com/help"
let kURLPolicy = "https://www.arrivy.com/privacy"
let kURLService = "https://www.arrivy.com/terms"

let kTXTLogout = 0
let kTXTLogin = 1
let kAuthToken = "Token"
let kAuthKey = "Key"
let kCompanyOwner = "Owned"
let kOwnedCompany = "Owned Company"
let kNotOwnedCompany = "Not Owned Company"

let kEntityID = "Entity"
let kTaskID = "TaskID"
let kLocationService = "LocationService"
//let kTurnOn = "TurnOn"
//let kTUrnOff = "TurnOff"
let kLastLon = "lastLon"
let kLastLat = "lastLat"

let kSelectCalendar = "Select Calendar"
let kDateSelected = "Date"
let kCurrentStauts = "CurrentStatus"

let kFullName = "FullName"
let kTimeInterval = "TimeInterval"

//Text
let kTXTAlertBody = "Please turn on location service"
let kTXTUserID = "UserID"
let kTXTStatus = "Status"
let kEmptyString = ""
let kJSONNull = "<null>"
let kTXTComplete = "COMPLETE"
let kTXTCompleted = "COMPLETED"
let kTXTCancelled = "CANCELLED"

let kTXTStatuss = "Status               "
let kTXTDetail = "Details              "
let kTXTNotStart = "Not Start"
let kTXTOnOurWay = "On our way"
let kTXTTaskStarted = "Task started"
let kTXTTaskComplete = "Task complete"
let kTXTTaskCancel = "Task cancel"
let kTXTException = "Exception"
let kTXTCustom = "Custom"

let kNotStarted = "NOTSTARTED"
let kEnRoute = "ENROUTE"
let kStarted = "STARTED"
let kComplete = "COMPLETE"
let kCancelled = "CANCELLED"
let kException = "EXCEPTION"
//let kContact = "CONTACT" 
let kCustom = "CUSTOM"

//Alert Text
let kAlertEmail = "Email invalid. Please check again!"
let kAlertTextEmpty = "You must fill enought text field. Please check again!"
let kAlertForgetPass = "An email is sent with instructions on how to reset you password."
let kAlertForgetPassFaild = "Reset password occur error. Please check again"
let kAlertPasswordWrong = "Email or Password are wrong. Please check again"

//JSON Key
let kJSCEntityID = "company_entity_id"
let kJSCMessage = "message"

let kJSKUserID = "userId"
let kJSKUsername = "userName"

let kJSCEntity  = "entity"
let kJSCLatitude = "lat"
let kJSCLongtitude = "lng"
let kJSCTime = "time"
let kJSCCity = "city"
let kJSCStreet = "street"
let kJSCCountry = "country"
let kJSCMeta = "meta"

let kJSCEmail = "email"
let kJSCPassword = "password"
let kJSCFullname = "fullname"

let kJSCAuthKeys = "auth_keys"
let kJSCAuthKey = "auth_key"
let kJSCAuthToken = "auth_token"
let kJSCOwned = "company_owned"

let KJSCImagePath = "image_path"
let KJSCEmergency = "emergency"

let kJSCCustomer = "customer"
let kJSCStatus = "status"
let kJSCAddress = "customer_address"
let kJSCStartTime = "start_datetime"
let kJSCEndTime = "end_datetime"
let kJSCTaskTitle = "title"
let kJSCExtraField = "extra_fields"
let kJSCCustomerAddress = "customer_address_line_1"
let kJSCCustomerAddress2 = "customer_address_line_2"
let kJSCDestintionAddress = "destination_address_1"
let kJSCDestintionAddress2 = "destination_address_2"
let kJSCPrice = "price"
let kJSCDetail = "details"
let kJSCName = "customer_name"
let kJSCID = "id"
let kJSCPhone = "customer_phone"
let kJSCCustomerEmail = "customer_email"

let kJSCHourRate = "hourly_rate"
let kJSCPaymentStatus = "payment_status"
let kJSCCurrency = "currency"

let kJSCEstimate = "estimate"

//View controller
let kVCMainTabBar = "TrackTabBarController"
let kVCRegister = "RegisterViewController"
let kVCCalendar = "CalendarViewController"
let kVCDetailTask = "TaskDetailViewController"
let kVCForgetPass = "ForgetPassViewController"
let kVCLogin = "LoginViewController"
let kVCWebView = "WebViewController"

let kDetailView = "DetailView"
let kStatusView = "StatusView"

//color
let kColorGrayBorder = UIColor(red: 204.0/255.0, green: 204.0/255.0, blue: 204.0/255.0, alpha: 1.0)
let kColorMeadow = UIColor(red: 35.0/255.0, green: 156.0/255.0, blue: 130.0/255.0, alpha: 1.0)
let kColorCinnabar = UIColor(red: 266.0/255.0, green: 80.0/255.0, blue: 65.0/255.0, alpha: 1.0)
let kColorGreen = UIColor(red: 11.0/255.0, green: 211.0/255.0, blue: 24.0/255.0, alpha: 1.0)
let kColorBlue = UIColor(red: 29.0/255.0, green: 119.0/255.0, blue: 239.0/255.0, alpha: 1.0)
let kColorBlueSecond = UIColor(red: 129.0/255.0, green: 243.0/255.0, blue: 253.0/255.0, alpha: 1.0)
let kColorCinnabarSecond = UIColor(red: 266.0/255.0, green: 80.0/255.0, blue: 65.0/255.0, alpha: 0.8)
let kColorPink = UIColor(red: 255.0/255.0, green: 73.0/255.0, blue: 129.0/255.0, alpha: 0.8)
let kColorGray = UIColor(red: 230.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, alpha: 0.8)

//cell
let kCELLTask = "TaskCell"
