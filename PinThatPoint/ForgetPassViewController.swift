//
//  ForgetPassViewController.swift
//  PinThatPoint
//
//  Created by Apple on 10/11/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class ForgetPassViewController: UIViewController {

    
    @IBOutlet weak var btnConfirm: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
    }
    
    func configUI() {
        txtEmail.layoutIfNeeded()
        txtEmail.layer.cornerRadius = txtEmail.frame.size.height/2
        self.hideKeyboardWhenTappedAround()
    }
    
    func validEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func sendEmail(email : String, complition:@escaping ()->()) {
        let parameter = [kJSCEmail : email]
        SVProgressHUD.show()
        btnConfirm.isUserInteractionEnabled = false
        Alamofire.request(kAPIForgetPass, method: .post, parameters: parameter, encoding: URLEncoding.default).responseJSON {
            response in
            if let json = response.result.value as? NSDictionary {
                if let message = json[kJSCMessage] as? String, message == kAlertForgetPass {
                    self.showAlert(title: kEmptyString, message: kAlertForgetPass)
                }
                else {
                    self.showAlert(title: kEmptyString, message: kAlertForgetPassFaild)
                }
            }
            complition()
        }
    }

    @IBAction func clickedBtnConfirm(_ sender: AnyObject) {
        let email = txtEmail.text!
        if(email == kEmptyString) {
            self.showAlert(title: kEmptyString, message: kAlertTextEmpty)
            return
        }
        if(!validEmail(testStr: email)) {
            self.showAlert(title: kEmptyString, message: kAlertEmail)
            return
        }
        
        sendEmail(email: txtEmail.text!) { 
            SVProgressHUD.popActivity()
            self.btnConfirm.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func clickedBtnBack(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}
