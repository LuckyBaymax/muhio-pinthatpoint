//
//  WebViewController.swift
//  PinThatPoint
//
//  Created by Apple on 10/27/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var web: UIWebView!
    
    var urlStr : String!
    var navTitle : String!
    override func viewDidLoad() {
        lblTitle.text = navTitle
        super.viewDidLoad()
        web.loadRequest(URLRequest(url: URL(string: urlStr)!))
    }

    @IBAction func clickedBtnBack(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
   

}
