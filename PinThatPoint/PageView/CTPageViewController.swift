//
//  CTPageViewController.swift
//  PinThatPoint
//
//  Created by Michael Lee on 1/31/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class CTPageViewController: UIPageViewController, UIPageViewControllerDataSource {
    
    var childPages: [UIViewController] = [UIViewController].init() {
        didSet {
            
            for i in 0 ..< self.childPages.count {
                
                self.addChildViewController(childPages[i])
                childPages[i].view.tag = i
            }

            let initialViewController = childPages[0]
            let viewControllers = Array.init(arrayLiteral: initialViewController)
            self.setViewControllers(viewControllers, direction: .forward, animated: false, completion: nil)
            self.dataSource = self
        }
    }
    
    convenience init(transitionStyle: UIPageViewControllerTransitionStyle, navOrientaion: UIPageViewControllerNavigationOrientation, options: [String : Any]? = nil) {
        
        self.init(transitionStyle: transitionStyle, navigationOrientation: navOrientaion, options: options)
    }

    public func getCurrentPage() -> Int {
    
        let curViewController = self.viewControllers?[0]
        let index = curViewController?.view.tag
        return index!
    }

    public func goto(page index: Int, animated: Bool) {
    
        let curPage = self.getCurrentPage()
        let animationDirection: UIPageViewControllerNavigationDirection = curPage > index ? .reverse : .forward
        self.goto(page: index, transitionDirection: animationDirection, animated: animated)
    }
    
    public func goto(page index: Int, transitionDirection: UIPageViewControllerNavigationDirection, animated: Bool) {
    
        let page = self.childPages[index]
        self.setViewControllers([page], direction: transitionDirection, animated: animated) { (finished) in
        
            DispatchQueue.main.async {
                self.setViewControllers([page], direction: transitionDirection, animated: animated, completion: nil)
            }
        }
    }
    
    // MARK: - UIPageViewController Datasource
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        var index = self.getCurrentPage()
        
        if index == 0 {
            return nil
        }
        
        index -= 1
        
        return self.childPages[index]
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        var index = self.getCurrentPage()
        
        index += 1
        
        if index == self.childPages.count {
            return nil
        }
        
        return self.childPages[index]
    }
}
