//
//  TrackTabBarController.swift
//  PinThatPoint
//
//  Created by Apple on 10/1/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class TrackTabBarController: UITabBarController {

    var entites = [Entity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.addSeparator()
        
        let trackItem = self.tabBar.items?[0]
        trackItem?.titlePositionAdjustment = UIOffsetMake(0, -5.0);
        
        let taskItem = self.tabBar.items?[1]
        taskItem?.titlePositionAdjustment = UIOffsetMake(0, -5.0);
        
        let settingItem = self.tabBar.items?[2]
        settingItem?.titlePositionAdjustment = UIOffsetMake(0, -5.0);
    }

    /*
    func addSeparator() {
        if let items = self.tabBar.items {
            let height = self.tabBar.bounds.height
            let numItems = CGFloat(items.count)
            let itemSize = CGSize(
                width: tabBar.frame.width / numItems,
                height: tabBar.frame.height)
            
            for (index, _) in items.enumerated() {
                if index > 0 {
                    let xPosition = itemSize.width * CGFloat(index)
                    let separator = UIView(frame: CGRect(
                        x: xPosition, y: 0, width: 0.5, height: height))
                    separator.backgroundColor = UIColor.white
                    tabBar.insertSubview(separator, at: 1)
                }
            }
        }
    }*/
    
    func setColors() {
        let numberOfItems = CGFloat((self.tabBar.items!.count))
        let tabBarItemSize = CGSize(width: (self.tabBar.frame.width) / numberOfItems,
                                    height: (self.tabBar.frame.height))
        self.tabBar.frame.size.width = self.view.frame.width + 4
        self.tabBar.frame.origin.x = -2
        
        self.tabBar.selectionIndicatorImage
            = UIImage.imageWithColor(color: kColorMeadow,
                                     size: tabBarItemSize).resizableImage(withCapInsets: .zero)
        self.tabBar.tintColor = UIColor.white
    }

}

extension UIImage
{
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage
    {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
