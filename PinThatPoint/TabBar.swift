//
//  TabBar.swift
//  PinThatPoint
//
//  Created by Apple on 10/1/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class TabBar: UITabBar {
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = 75
        return sizeThatFits
    }
}


