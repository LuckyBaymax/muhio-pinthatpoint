//
//  LoginViewController.swift
//  PinThatPoint
//
//  Created by Apple on 10/1/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import Foundation

class LoginViewController: UIViewController {
    
    @IBOutlet weak var googleView: UIView!
    @IBOutlet weak var FBView: UIView!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnForgetPass: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var fullName : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
        txtPassword.delegate = self
    }
    
    func configUI() {
        self.txtEmail.layoutIfNeeded()
        self.txtPassword.layoutIfNeeded()
        self.txtEmail.layer.cornerRadius = self.txtEmail.layer.frame.size.height/2
        self.txtPassword.layer.cornerRadius = self.txtPassword.layer.frame.size.height/2
        self.txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(5.0, 0.0, 0.0)
        self.txtPassword.layer.sublayerTransform = CATransform3DMakeTranslation(5.0, 0.0, 0.0)
        self.hideKeyboardWhenTappedAround()
        
        self.FBView.layoutIfNeeded()
        self.googleView.layoutIfNeeded()
        self.FBView.layer.cornerRadius = self.FBView.frame.size.height/2
        self.googleView.layer.cornerRadius = self.googleView.frame.size.height/2
        
        //test
        txtEmail.text = ""
        txtPassword.text = ""
    }
    
    //MARK: Login
    func login(email : String!, password : String!, complterion:@escaping ()->()) {
        let parameters: HTTPHeaders = [
            kJSCEmail: email,
            kJSCPassword: password
        ]

        btnLogin.isUserInteractionEnabled = false
        self.btnRegister.isUserInteractionEnabled = false
        self.btnForgetPass.isUserInteractionEnabled = false
        SVProgressHUD.show()
        
    
        Alamofire.request(kAPILogin, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            print("login response: \(response.result.value)")
            if let json = response.result.value as? NSDictionary {
                if let userID = json[kJSKUserID] as? Int {
                    //save userID to UserDefault
                    UserDefaults.standard.setValue(userID, forKey: kTXTUserID)
                    UserDefaults.standard.setValue(kTXTLogin, forKey: kTXTStatus)
                    if let fullName = json[kJSKUsername] as? String, fullName != "" {
                        self.fullName = fullName
                    }
                    complterion()
                }
                else {
                    self.showAlert(title: kEmptyString, message: kAlertPasswordWrong)
                    self.btnLogin.isUserInteractionEnabled = true
                    self.btnRegister.isUserInteractionEnabled = true
                    self.btnForgetPass.isUserInteractionEnabled = true
                    SVProgressHUD.popActivity()
                }
            }
            //complterion()
        }
        
    }
    
    //MARK: Get Profile
    func getProfile(completion:@escaping ()->()) {
        Alamofire.request(kAPIProfile, method: .get).responseJSON{ response in
            if let json = response.result.value as? NSDictionary {
                print("getProfile, response: \(json)")
                if let authKeys = json[kJSCAuthKeys] as? NSDictionary {
                    if let auth_key = authKeys[kJSCAuthKey] as? String {
                        UserDefaults.standard.setValue(auth_key, forKey: kAuthKey)
                    }
                    if let auth_token = authKeys[kJSCAuthToken] as? String {
                        UserDefaults.standard.setValue(auth_token, forKey: kAuthToken)
                    }
                }
                if let owned_company_id = json["owned_company_id"] as? Int64 {
                    UserDefaults.standard.setValue(owned_company_id, forKey:"owned_company_id")
                }
                
                if let owner = json["owner"] as? Int64 {
                    UserDefaults.standard.setValue(owner, forKey:"owner")
                }
                
                
                // get data to check owned
                if let _ = json[kJSCOwned] as? Int {
                    UserDefaults.standard.setValue(kOwnedCompany, forKey:kCompanyOwner)
                } else {
                    UserDefaults.standard.setValue(kNotOwnedCompany, forKey:kCompanyOwner)
                }
                //get Image_path
                if let image_path = json[KJSCImagePath] as? String {
                    UserDefaults.standard.setValue(image_path, forKey:KJSCImagePath)
                }
                //get Emergency
                if let emergency = json[KJSCEmergency] as? String {
                    UserDefaults.standard.setValue(emergency, forKey:KJSCEmergency)
                }
                //get Detail
                if let details = json[kJSCDetail] as? String {
                    UserDefaults.standard.setValue(details, forKey:kJSCDetail)
                }
                //get entity id
                if let company_entity_id = json[kJSCEntityID] as? Int64 {
                    UserDefaults.standard.setValue(company_entity_id, forKey:kEntityID)
                } else {
                    UserDefaults.standard.setValue(-1, forKey:kEntityID)
                }
                // get default_entity_id
                if let default_entity_id = json["default_entity_id"] as? Int64 {
                    UserDefaults.standard.set(default_entity_id, forKey: "default_entity_id")
                } else {
                    UserDefaults.standard.set(-1, forKey: "default_entity_id")
                }
                //company_owned
                if let company_owned = json["company_owned"] as? Bool {
                    UserDefaults.standard.set(company_owned, forKey: "company_owned")
                } else {
                    UserDefaults.standard.set(false, forKey: "company_owned")
                }
                //if company account TODO: refactor this
                if let fullname = json["fullname"] as? String {
                    UserDefaults.standard.setValue(fullname, forKey: "fullname")
                }
                if let image_path = json["image_path"] as? String {
                    UserDefaults.standard.setValue(image_path, forKey: "image_path")
                }
                if let details = json["details"] as? String {
                    UserDefaults.standard.setValue(details, forKey: "details")
                }
                if let company_type = json["company_type"] as? String {
                    UserDefaults.standard.setValue(company_type, forKey: "company_type")
                }
                
                completion()
            }
        }
    }
    
    //MARK: Validate
    func validEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //MARK: Event
    @IBAction func ClickedBtnLogin(_ sender: AnyObject) {
        let email = txtEmail.text
        let password = txtPassword.text
        
        if(email == kEmptyString || password == kEmptyString) {
            showAlert(title: kEmptyString, message: kAlertTextEmpty)
            return
        }
        if(!validEmail(testStr: email!)) {
            showAlert(title: kEmptyString, message: kAlertEmail)
            return
        }
        
        self.login(email: email, password: password, complterion: {
            self.getProfile {
                self.btnLogin.isUserInteractionEnabled = true
                self.btnRegister.isUserInteractionEnabled = true
                self.btnForgetPass.isUserInteractionEnabled = true
                SVProgressHUD.popActivity()
                
                let mainTabBar : TrackTabBarController! = self.storyboard?.instantiateViewController(withIdentifier:kVCMainTabBar) as! TrackTabBarController
                //let trackVC = mainTabBar.viewControllers?[0] as! TrackViewController
                //trackVC.name = self.fullName
                UserDefaults.standard.setValue(self.fullName, forKey: kFullName)
                self.present(mainTabBar, animated: true, completion: nil)
            }
        })
    }

    @IBAction func clickedBtnRegister(_ sender: AnyObject) {
        let registerVC = self.storyboard?.instantiateViewController(withIdentifier:kVCRegister) as! RegisterViewController
        self.present(registerVC, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnForgetPass(_ sender: AnyObject) {
        let forgetPassVC = self.storyboard?.instantiateViewController(withIdentifier: kVCForgetPass) as! ForgetPassViewController
        self.present(forgetPassVC, animated: true, completion: nil)
    }
    
}

extension LoginViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtPassword.resignFirstResponder()
        let email = txtEmail.text
        let password = txtPassword.text
        
        if(email == kEmptyString || password == kEmptyString) {
            showAlert(title: kEmptyString, message: kAlertTextEmpty)
            return false
        }
        if(!validEmail(testStr: email!)) {
            showAlert(title: kEmptyString, message: kAlertEmail)
            return false
        }
        
        self.login(email: email, password: password, complterion: {
            self.getProfile {
                self.btnLogin.isUserInteractionEnabled = true
                self.btnRegister.isUserInteractionEnabled = true
                self.btnForgetPass.isUserInteractionEnabled = true
                SVProgressHUD.popActivity()
                
                let mainTabBar : TrackTabBarController! = self.storyboard?.instantiateViewController(withIdentifier:kVCMainTabBar) as! TrackTabBarController
//                let trackVC = mainTabBar.viewControllers?[0] as! TrackViewController
//                trackVC.name = self.fullName
                UserDefaults.standard.setValue(self.fullName, forKey: kFullName)
                self.present(mainTabBar, animated: true, completion: nil)
            }
        })

        return true
    }
}
