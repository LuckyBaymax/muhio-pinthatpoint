

import UIKit
import FSCalendar

class CalendarViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate {

    @IBOutlet weak var calendarHeightContraint: NSLayoutConstraint!
    @IBOutlet weak var calendar: FSCalendar!
    
    private let Localformatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = NSTimeZone.local
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return formatter
    }()
    private let gregorian: NSCalendar! = NSCalendar(calendarIdentifier:NSCalendar.Identifier.gregorian)
    
    @IBAction func clickedBtnBack(_ sender: AnyObject) {
        UserDefaults.standard.setValue(false, forKey: kSelectCalendar)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calendar.dataSource = self
        calendar.delegate = self
        calendar.backgroundColor = UIColor.white
        calendar.scopeGesture.isEnabled = false
        calendar.todayColor = kColorCinnabar
        calendar.headerTitleTextSize = 10
        calendar.titleTextSize = 10
        
        let today = Localformatter.string(from: Date())
        self.calendar.appearance.caseOptions = [.headerUsesUpperCase,.weekdayUsesSingleUpperCase]
        self.calendar.select(self.Localformatter.date(from: today)!)
        print("local today is: \(today)")
    
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        NSLog("change page to \(self.Localformatter.string(from: calendar.currentPage))")
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date) {
        NSLog("calendar did select date \(self.Localformatter.string(from: date))")
    
        UserDefaults.standard.setValue(true, forKey: kSelectCalendar)
        UserDefaults.standard.setValue(date, forKey: kDateSelected)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
         calendar.frame = CGRect(x: 0, y: self.navigationController!.navigationBar.frame.maxY, width: bounds.width, height: bounds.height)
        calendar.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, imageFor date: Date) -> UIImage? {
        let day: Int! = self.gregorian.component(.day, from: date)
        return [13,24].contains(day) ? UIImage(named: "icon_cat") : nil
    }
   

}
