//
//  Task.swift
//  PinThatPoint
//
//  Created by Apple on 10/7/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class TitleValue {
    var title = ""
    var value = ""
    
    init(title: String, value: String) {
        self.title = title
        self.value = value
    }
}

class Task: NSObject {
    var customerAddress = ""
    var customerAddress1 = ""
    var customerAddress2 = ""
    var paymentInstructions = ""
    var status = ""
    var startTimeUTC = ""
    var endTimeUTC = ""
    var startTimeLocal = ""
    var endTimeLocal = ""
    var taskTitle = ""
    var price: Double = 0.0
    var taskDetail = ""
    var destinationAddress1 = ""
    var destinationAddress2 = ""
    var destinationAddress3 = ""
    var destinationAddress4 = ""
    var destinationAddress5 = ""
    var customerName = ""
    var customerCity = ""
    var customerCompanyName = ""
    var customerCountry = ""
    var customerPhone = ""
    var customerPhone2 = ""
    var cusEmail = ""
    var customerState = ""
    var customerZip = ""
    var customerNotes = ""
    var customerId = ""
    var customerLat = 0.0
    var customerLon = 0.0
    
    var paymentStatus = ""
    var currency = ""
    var hourRate: Double = 0.0
    var taskID: Int64 = 0
    var taskColor = ""
    var values = [TitleValue]()
    var entityIds = [Int64]()
    var extraField: NSMutableDictionary = [:]
    var customerValues = [TitleValue]()
    
    init(dict : NSDictionary) {
        super.init()
        
        if let _status = dict["status"] as? String {
            status = _status
        }
        if let eids = dict["entity_ids"] as? NSArray {
            for e in eids {
                if let int64 = e as? Int64 {
                    entityIds.append(int64)
                }
            }
        }
        
        if let extraFieldConst = dict["extra_fields"] as? NSDictionary {
            extraField = NSMutableDictionary(dictionary: extraFieldConst)
            if let _price = extraField["price"] as? Double {
                price = _price
            }
            if let _paymentStatus = extraField["payment_status"] as? String {
                paymentStatus = _paymentStatus
            }
            if let _currency = extraField["currency"] as? String {
                currency = _currency
            }
            if let _hourRate = extraField["hourly_rate"] as? Double {
                hourRate = _hourRate
            }
            if let _destinationAddress = extraField["destination_address_1"] as? String {
                destinationAddress1 = _destinationAddress
                extraField.removeObject(forKey: "destination_address_1")
            }
            if let _destinationAddress2 = extraField["destination_address_2"] as? String {
                destinationAddress2 = _destinationAddress2
                extraField.removeObject(forKey: "destination_address_2")
            }
            if let _destinationAddress3 = extraField["destination_address_3"] as? String {
                destinationAddress3 = _destinationAddress3
                extraField.removeObject(forKey: "destination_address_3")
            }
            if let _destinationAddress4 = extraField["destination_address_4"] as? String {
                destinationAddress4 = _destinationAddress4
                extraField.removeObject(forKey: "destination_address_4")
            }
            if let _destinationAddress5 = extraField["destination_address_5"] as? String {
                destinationAddress5 = _destinationAddress5
                extraField.removeObject(forKey: "destination_address_5")
            }
            if let _paymentInstructions = extraField["payment_instructions"] as? String {
                paymentInstructions = _paymentInstructions
            }
            if let _taskColor = extraField["task_color"] as? String {
                taskColor = _taskColor
                extraField.removeObject(forKey: "task_color")
            }
        }
        //customer fields:
        if let _customerName = dict["customer_name"] as? String, _customerName != "" {
            customerName = _customerName
            customerValues.append(TitleValue(title: "Customer name", value: customerName))
        }
        if let _address = dict["customer_address"] as? String, _address != "" {
            customerAddress = _address
            //customerValues.append(TitleValue(title: "Customer address", value: customerAddress))
        }
        if let _address = dict["customer_address_line_1"] as? String, _address != "" {
            customerAddress1 = _address
            //customerValues.append(TitleValue(title: "Customer address 1", value: customerAddress1))
        }
        if let _address = dict["customer_address_line_2"] as? String, _address != "" {
            customerAddress2 = _address
            //customerValues.append(TitleValue(title: "Customer address 2", value: customerAddress2))
        }
        if let _email = dict["customer_email"] as? String, _email != ""  {
            cusEmail = _email
            customerValues.append(TitleValue(title: "Customer email", value: cusEmail))
        }
        if let _cusCity = dict["customer_city"] as? String, _cusCity != ""  {
            customerCity = _cusCity
            customerValues.append(TitleValue(title: "Customer city", value: customerCity))
        }
        if let _cusCName = dict["customer_company_name"] as? String, _cusCName != ""  {
            customerCompanyName = _cusCName
            customerValues.append(TitleValue(title: "Customer company name", value: customerCompanyName))
        }
        if let _cusCountry = dict["customer_country"] as? String, _cusCountry != ""  {
            customerCountry = _cusCountry
            customerValues.append(TitleValue(title: "Customer country", value: customerCountry))
        }
        if let _phone = dict["customer_phone"] as? String, _phone != "" {
            customerPhone = _phone
            customerValues.append(TitleValue(title: "Customer phone", value: customerPhone))
        }
        if let _phone = dict["customer_phone2"] as? String, _phone != "" {
            customerPhone2 = _phone
            customerValues.append(TitleValue(title: "Customer phone 2", value: customerPhone2))
        }
        if let _state = dict["customer_state"] as? String, _state != "" {
            customerState = _state
            customerValues.append(TitleValue(title: "Customer state", value: customerState))
        }
        if let _zip = dict["customer_zipcode"] as? String, _zip != "" {
            customerZip = _zip
            customerValues.append(TitleValue(title: "Customer zip code", value: customerZip))
        }
        if let _notes = dict["customer_notes"] as? String, _notes != "" {
            customerNotes = _notes
            customerValues.append(TitleValue(title: "Customer notes", value: customerNotes))
        }
        if let _id = dict["customer_id"] as? String, _id != "" {
            customerId = _id
        }
        if let locations = dict["customer_exact_location"] as? NSDictionary {
            if let lat = locations["lat"] as? Double, let lng = locations["lng"] as? Double {
                customerLat = lat
                customerLon = lng
                customerValues.append(TitleValue(title: "Customer location", value: "\(lat), \(lng)"))
            }
        }
        /*
        "customer_exact_location" =     {
            lat = "47.7156087";
            lng = "-122.1784239";
        };*/
        //other:
        //backend sends time in this format : "2016-11-17T17:00:00"
        if let _startTime = dict["start_datetime"] as? String {
            startTimeUTC = _startTime
        }
        if let _endTime = dict["end_datetime"] as? String {
            endTimeUTC = _endTime
        }
        if let _title = dict["title"] as? String {
            taskTitle = _title
        }
        if let _detail = dict["details"] as? String {
            taskDetail = _detail
        }
        if let _taskID = dict["id"] as? Int64 {
            taskID = _taskID
        }
       
        UserDefaults.standard.setValue(taskID, forKey: kTaskID) 
        convertDatesToLocal()
        addValuesInRightOrder()
    }
    
    func convertDatesToLocal() {
        // create dateFormatter with UTC time format
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!

        guard let sdate = dateFormatter.date(from: startTimeUTC), let edate = dateFormatter.date(from: endTimeUTC) else {
            return
        }
        //set local zone
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.local
        //set start date
        dateFormatter2.dateFormat = "MMM d h:mm a"
        startTimeLocal = dateFormatter2.string(from: sdate)
        //if same day, then end date without day.
        let userCalendar = Calendar.current
        if userCalendar.component(.day, from: sdate) == userCalendar.component(.day, from: edate) {
            dateFormatter2.dateFormat = "h:mm a"
        } else {
            dateFormatter2.dateFormat = "MMM d h:mm a"
        }
        endTimeLocal = dateFormatter2.string(from: edate)
    }
    
    func addValuesInRightOrder() {
        /*
         Display details and extra_fields in this order:
         destination_address_1
         destination_address_x (any other addresses to show)
         details
         Only show first few lines and click to expand
         rest of the fields
         show field names properly e.g.
         hourly_rate should be shown as Hourly rate: ....
         payment_status should be shown as Payment status: ....
         Keep a map of these. We will add more key names here.
         DONT show task_color field
         show all the customer fields once these extra_fields are shown
        */
        //firstly add fields from task:
        if destinationAddress1 != "" {
            values.append(TitleValue(title: "Destination address", value: destinationAddress1))
        }
        if destinationAddress2 != "" {
            values.append(TitleValue(title: "Destination address 2", value: destinationAddress2))
        }
        if destinationAddress3 != "" {
            values.append(TitleValue(title: "Destination address 3", value: destinationAddress3))
        }
        if destinationAddress4 != "" {
            values.append(TitleValue(title: "Destination address 4", value: destinationAddress4))
        }
        if destinationAddress5 != "" {
            values.append(TitleValue(title: "Destination address 5", value: destinationAddress5))
        }
        if taskDetail != "" {
            values.append(TitleValue(title: "Task details", value: taskDetail))
        }
        //secondly, all the extra_field fields
        for field in extraField {
            if let title = field.key as? String, let value = field.value as? String, value != "" {
                values.append(TitleValue(title: makeReadable(string: title), value: value))
            }
        }
        //last, customer_* fields
        values.append(contentsOf: customerValues)
    }
    
    func firstCharacterUpperCase(sentenceToCap: String) -> String {
        guard sentenceToCap != "" else {
            return ""
        }
        var s = sentenceToCap
        s.replaceSubrange(s.startIndex...s.startIndex, with: String(s[s.startIndex]).capitalized)
        return s
    }
    
    func makeReadable(string: String) -> String {
        return firstCharacterUpperCase(sentenceToCap: string.replacingOccurrences(of: "_", with: " "))
    }
}
