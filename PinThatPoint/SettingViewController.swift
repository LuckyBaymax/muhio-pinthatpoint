//
//  SettingViewController.swift
//  PinThatPoint
//
//  Created by Apple on 10/4/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD


class SettingViewController: UIViewController, TimeViewDelegate {
    
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var btnHelp: UIButton!
    @IBOutlet weak var btnPolicy: UIButton!
    @IBOutlet weak var btnService: UIButton!
    @IBOutlet weak var btnLogout: UIButton!
    
    let data : [String] = ["10 seconds","20 seconds","40 seconds", "1 minute", "2 minutes", "3 minutes", "5 minutes", "10 minutes"]
    var timeView : TimeView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        timeView = TimeView.createViewWithData(data: data)
        timeView.delegate = self
        self.hideKeyboardWhenTappedAround()
        
        if(UserDefaults.standard.value(forKey: kTimeInterval) == nil) {
            UserDefaults.standard.setValue(60, forKey: kTimeInterval)
        }
        updateTimeButton()
        txtTime.inputView = timeView
    }
    
    func updateTimeButton() {
        guard let time = UserDefaults.standard.value(forKey: kTimeInterval) as? Int else {
            return
        }
        switch time {
        case 10:
            self.txtTime.text = "10 second"
            break
        case 20:
            self.txtTime.text = "20 seconds"
            break
        case 40:
            self.txtTime.text = "40 seconds"
            break
        case 60:
            self.txtTime.text = "1 minutes"
            break
        case 120:
            self.txtTime.text = "2 minutes"
            break
        case 180:
            self.txtTime.text = "3 minutes"
            break
        case 300:
            self.txtTime.text = "5 minutes"
            break
        case 600:
            self.txtTime.text = "10 minutes"
            break
        default:
            break
        }
        
    }
    
    func updateTimeInterval(index : Int) {
        if (data[index] == "10 seconds") {
            UserDefaults.standard.setValue(10, forKey: kTimeInterval)
        }
        else if(data[index] == "20 seconds") {
            UserDefaults.standard.setValue(20, forKey: kTimeInterval)
        }
        else if(data[index] == "40 seconds") {
            UserDefaults.standard.setValue(40, forKey: kTimeInterval)
        }
        else if(data[index] == "40 seconds") {
            UserDefaults.standard.setValue(40, forKey: kTimeInterval)
        }
        else if(data[index] == "1 minute") {
            UserDefaults.standard.setValue(60, forKey: kTimeInterval)
        }
        else if(data[index] == "2 minutes") {
            UserDefaults.standard.setValue(120, forKey: kTimeInterval)
        }
        else if(data[index] == "3 minutes") {
            UserDefaults.standard.setValue(180, forKey: kTimeInterval)
        }
        else if(data[index] == "5 minutes") {
            UserDefaults.standard.setValue(300, forKey: kTimeInterval)
        }
        else if(data[index] == "10 minutes") {
            UserDefaults.standard.setValue(600, forKey: kTimeInterval)
        }
        // Post notification
        NotificationCenter.default.post(name: Notification.Name("refreshLocationIntevalChanged"), object: nil)
        updateTimeButton()
    }

    @IBAction func clickedBtnLogout(_ sender: AnyObject) {
        self.logout {
            SVProgressHUD.popActivity()
            self.setUserInteration(value: true)
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: kVCLogin)
            self.present(loginVC!, animated: true, completion: nil)
            UserDefaults.standard.setValue(kTXTLogout, forKey: kTXTStatus)
        }
    }
    
    func logout(completion:@escaping ()->()) {
        SVProgressHUD.show()
        setUserInteration(value: false)
        Alamofire.request(kAPILogout, method: .get).responseJSON {
            response in
            if let json = response.result.value {
                print(json)
            }
            NotificationCenter.default.post(name: Notification.Name("logout"), object: nil)
            for key in UserDefaults.standard.dictionaryRepresentation().keys {
                UserDefaults.standard.removeObject(forKey: key)
            }
            completion()
        }
    }
    
    func setUserInteration(value : Bool) {
        btnHelp.isUserInteractionEnabled = value
        btnPolicy.isUserInteractionEnabled = value
        btnService.isUserInteractionEnabled = value
        btnLogout.isUserInteractionEnabled = value
    }
    
    //MARK: Event
    @IBAction func clickedBtnHelp(_ sender: AnyObject) {
        goTo(urlStr: kURLHelp, navTitle: "Help Center")
    }
    
    @IBAction func clickedBtnService(_ sender: AnyObject) {
        goTo(urlStr: kURLService, navTitle: "Terms of Service")
    }
    
    @IBAction func clickedBtnPolicy(_ sender: AnyObject) {
        goTo(urlStr: kURLPolicy, navTitle: "Privacy Policy")
    }
    
    func goTo(urlStr : String, navTitle : String) {
        let webVC = self.storyboard?.instantiateViewController(withIdentifier: kVCWebView) as! WebViewController
        webVC.urlStr = urlStr
        webVC.navTitle = navTitle
        self.navigationController?.pushViewController(webVC, animated: true)
    }
    
    func didDone(selected: Int) {
        self.updateTimeInterval(index: selected)
        self.txtTime.resignFirstResponder()
    }
    
    func didCancel() {
        self.txtTime.resignFirstResponder()
    }
    

}

//extension SettingViewController : UITableViewDataSource, UITableViewDelegate {
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return data.count
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
//        
//        cell.lblTitle.text = data[indexPath.row]
//        
//        return cell
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        tableView.deselectRow(at: indexPath, animated: false)
//        self.updateTimeInterval(index: indexPath.row)
//        UIView.animate(withDuration: 0.3) { 
//            self.tbvSetting.isHidden = !self.tbvSetting.isHidden
//        }
//    }
//
//}
