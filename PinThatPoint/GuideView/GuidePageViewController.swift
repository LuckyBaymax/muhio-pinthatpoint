//
//  GuidePageViewController.swift
//  PinThatPoint
//
//  Created by Michael Lee on 1/31/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class GuidePageViewController: UIViewController {

    var pageNum = 0
    
    // MARK: - Outlets
    @IBOutlet weak var imgViewContent: UIImageView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        var imageName: String = String.init()
        switch pageNum {
            
        case 0:
            imageName = "img-guide-bg-2"
            break
        case 1:
            imageName = "img-guide-bg-3"
            break
        case 2:
            imageName = "img-guide-bg-4"
            break
        default:
            break
        }
        
        if !imageName.isEmpty {
            self.imgViewContent.image = UIImage.init(named: imageName)
        }
    }
    override func viewWillLayoutSubviews() {
        
        super.viewWillLayoutSubviews()
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
    }

}
