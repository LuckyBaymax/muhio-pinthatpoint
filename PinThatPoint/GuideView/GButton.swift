//
//  GButton.swift
//  PinThatPoint
//
//  Created by Michael Lee on 1/31/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

class GButton: UIButton {

    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = 4.0
        self.layer.borderColor = CLR_BTN_GUIDE.cgColor
        self.layer.borderWidth = 1.0
    }

}
