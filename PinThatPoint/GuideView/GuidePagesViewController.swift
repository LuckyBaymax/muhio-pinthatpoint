//
//  GuidePagesViewController.swift
//  PinThatPoint
//
//  Created by Michael Lee on 1/31/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import UIKit

let totalPage = 3

class GuidePagesViewController: UIViewController, UIPageViewControllerDelegate {
    
    var controlsPlaced = false
    var selPage = 0
    var pageViewController = CTPageViewController.init(transitionStyle: .scroll, navOrientaion: .horizontal, options: nil)
    
    // MARK: - Outlets
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    @IBOutlet weak var btnSkip: GButton!
    @IBOutlet weak var btnLeft: GButton!
    @IBOutlet weak var btnRight: GButton!
    
    // MARK: - UIView Lifecycle and Inits
    
    // MARK: - UIView Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.initVariables()
        self.initControls()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        self.controlsPlaced = true
    }
    
    override func viewWillLayoutSubviews() {
        
        super.viewWillLayoutSubviews()
        
        if !self.controlsPlaced {
            self.layoutControls()
        }
    }
    
    // MARK: - Init Variables
    func initVariables() {
        
        self.controlsPlaced = false
        
        /**
         *  Initialize Child View Controllers
         */
        
        var childPages = [UIViewController].init()
        
        for i in 0 ..< 3 {
        
            let pageView = self.storyboard?.instantiateViewController(withIdentifier: "SB_GUIDE_PAGE") as! GuidePageViewController
            pageView.pageNum = i
            childPages.append(pageView)
        }
        
        /**
         *  Add Page View Controller
         */
        
        self.pageViewController.delegate = self
        self.pageViewController.childPages = childPages

        self.addChildViewController(self.pageViewController)
        
        self.contentView.addSubview((self.pageViewController.view)!)
        
        /**
         *  Select keypad tab
         */
        
        self.selPage = 0
    }
    
    // MARK: - Init Controls
    func initControls() {
        
//        self.select(page: selPage)
//        self.pageViewController.goto(page: selPage, animated: true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: - Layout Controls
    func layoutControls() {
        
        /**
         *  Set page view controller frame
         */
        
        self.pageViewController.view.frame = self.contentView.bounds
        self.pageViewController.view.layoutIfNeeded()
    }
    
    // MARK: - Actions
    
    @IBAction func actionLeft(_ sender: Any) {
        
        let currentPage = pageViewController.getCurrentPage()
        
        if currentPage == 0 {
            _ = self.navigationController?.popViewController(animated: true)
        }
        else {
            self.pageViewController.goto(page: currentPage - 1, animated: true)
            self.select(page: currentPage - 1)
        }
    }
    
    @IBAction func actionRight(_ sender: Any) {
        
        let currentPage = pageViewController.getCurrentPage()
        
        if currentPage == totalPage - 1 {
            self.performSegue(withIdentifier: "SG_LOGIN", sender: nil)
        }
        else {
            self.pageViewController.goto(page: currentPage + 1, animated: true)
            self.select(page: currentPage + 1)
        }
    }
    
    // MARK: - Action Process
    func select(page: Int) {
        
        let currentPage = pageViewController.getCurrentPage()
        
        self.pageControl.currentPage = page
        self.btnSkip.isHidden = currentPage == totalPage - 1
        
        var leftTitle:String = "BACK"
        var rightTitle:String = "NEXT"
        
        if currentPage == totalPage - 1 {
            
            leftTitle = "BACK"
            rightTitle = "START"
            self.btnRight.setTitleColor(UIColor.white, for: .normal)
            self.btnRight.backgroundColor = CLR_BTN_GUIDE
        }
        else {
            self.btnRight.setTitleColor(CLR_BTN_GUIDE, for: .normal)
            self.btnRight.backgroundColor = UIColor.white
        }
        
        self.btnLeft.setTitle(leftTitle, for: .normal)
        self.btnRight.setTitle(rightTitle, for: .normal)
    }

    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        let currentPage = self.pageViewController.getCurrentPage()
        self.select(page: currentPage)
    }
}
