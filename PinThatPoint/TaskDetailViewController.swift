//
//  TaskDetailViewController.swift
//  PinThatPoint
//
//  Created by Apple on 10/8/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import MessageUI
import Alamofire
import SVProgressHUD
import ESPullToRefresh

class Status {
    var id = ""
    var type = ""
    var reporterName = ""
    var reporterId: Int64 = 0
    var timeUTC = Date(timeIntervalSince1970: 0) //good old times
    var timeLocal = ""
    var visibleToCustomer = false
    var files = [String]()
    var notes = ""
    
    init(id: String, type: String, timeLocal: String, reporterName: String, reporterId: Int64) {
        self.id = id
        self.type = type
        self.reporterName = reporterName
        self.reporterId = reporterId
    }
    
    init() {
        
    }
}

enum CurrentViewName {
    case statuses
    case details
}

enum StatusName: String {
    case onOurWay = "ENROUTE"
    case notStart = "NOTSTARTED"
    case start = "STARTED"
    case complete = "COMPLETE"
    case cancel = "CANCELLED"
    case exception = "EXCEPTION"
    case custom = "CUSTOM"
}

class TaskDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var constraintTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constraintViewCallSmsMailHeight: NSLayoutConstraint!
    @IBOutlet weak var viewCallSmsMail: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnSMS: UIButton!
    @IBOutlet weak var btnMail: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnEstimate: UIButton!
    @IBOutlet weak var segmentView: UIView!
    @IBOutlet weak var btnETA: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNavTit: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblCrew: UILabel!
    @IBOutlet weak var viewCircles: UIView!
    @IBOutlet weak var buttonContact: UIButton!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var textViewContact: UITextView!
    @IBOutlet weak var viewContact: UIView!
    
    //Status view
    @IBOutlet weak var constraintSecondRowRight: NSLayoutConstraint!
    @IBOutlet weak var constraintSecondRowLeft: NSLayoutConstraint!
    @IBOutlet weak var constraintSecondRowTop: NSLayoutConstraint!
    @IBOutlet weak var constraintFirstRowRight: NSLayoutConstraint!
    @IBOutlet weak var constraintFirstRowLeft: NSLayoutConstraint!
    @IBOutlet weak var constraintButtonsHeight: NSLayoutConstraint!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var constraintButtonsWidth: NSLayoutConstraint!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblException: UILabel!
    @IBOutlet weak var lblCancel: UILabel!
    @IBOutlet weak var lblComplete: UILabel!
    @IBOutlet weak var lblStart: UILabel!
    @IBOutlet weak var lblOnway: UILabel!
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var btnException: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnTaskComplete: UIButton!
    @IBOutlet weak var btnTaskStart: UIButton!
    @IBOutlet weak var btnOurWay: UIButton!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var indicator: UIView!
    @IBOutlet weak var statusViewButton: UIButton!
    @IBOutlet weak var detailsViewButton: UIButton!
    
    @IBOutlet weak var notesButton: UIButton!

    var status : StatusName?
    var statuses = [Status]()
    var currentView = CurrentViewName.statuses //opens status view on load.
    var spaceBetweenCircles: CGFloat = 0.0
    var circleSize: CGFloat = 0.0
    var task : Task!
    var entites = [Entity]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isHidden = true
        statusView.isHidden = false
        notesButton.layer.borderColor = kColorMeadow.cgColor
        notesButton.layer.borderWidth = 1.0
        notesButton.clipsToBounds = true
        notesButton.layer.cornerRadius = 2.0
        notesButton.setTitle("Notes", for: .normal)
        notesButton.setTitleColor(kColorMeadow, for: .normal)
        
        //set sizes
        let screenWidth = UIScreen.main.bounds.width
        let kCirclesInRow: CGFloat = 3
        let kSpaceBetweenCircles: CGFloat = 0.25
        circleSize = screenWidth / ((kCirclesInRow + 1.0) * kSpaceBetweenCircles + kCirclesInRow)
        spaceBetweenCircles = circleSize * kSpaceBetweenCircles
        //circles
        constraintButtonsWidth.constant = circleSize
        constraintButtonsHeight.constant = circleSize
        //first row
        constraintFirstRowLeft.constant = spaceBetweenCircles
        constraintFirstRowRight.constant = spaceBetweenCircles
        //second row
        constraintSecondRowTop.constant = spaceBetweenCircles
        constraintSecondRowLeft.constant = spaceBetweenCircles
        constraintFirstRowRight.constant = spaceBetweenCircles
        //third row:
        //for the future.
        configUI()
        self.scrollView.es_addPullToRefresh {
            [weak self] in
            self?.scrollView.isUserInteractionEnabled = false
            self?.updateTask()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func updateTask() {
        Alamofire.request("https://www.arrivy.com/api/tasks/\(self.task.taskID)", method:.get, parameters: [:], encoding: URLEncoding.default).responseJSON {
            response in
            print("func updateTask, response: \(response)")
            if let dict = response.result.value as? NSDictionary {
                self.task = Task(dict: dict)
                self.configUI()
            } else {
                self.scrollView.es_stopPullToRefresh(completion: true)
                self.scrollView.isUserInteractionEnabled = true
            }
        }
    }
    
    func configUI() {
        //contact vieew 
        viewContact.layer.borderWidth = 1.0
        viewContact.layer.borderColor = kColorMeadow.cgColor
        viewContact.layer.cornerRadius = 5.0
        viewContact.clipsToBounds = true
        textViewContact.layer.borderWidth = 1.0
        textViewContact.layer.borderColor = kColorMeadow.cgColor
        viewContact.isHidden = true
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 500.0
        print("table contains: ")
        for e in task.values {
            print("\(e.title) : \(e.value)")
        }
        tableView.reloadData()
        
        if(task.status != "") {
            UserDefaults.standard.setValue(task.status, forKey: kCurrentStauts)
        }
        let weDontHavePhone = (task.customerPhone == "")
        let weDontHaveMail = (task.cusEmail == "")
        btnCall.isHidden = weDontHavePhone
        btnSMS.isHidden = weDontHavePhone
        btnMail.isHidden = weDontHaveMail
        if (weDontHavePhone && weDontHaveMail) {
            constraintViewCallSmsMailHeight.constant = 20
        }
        
        lblName.text        = task.customerName
        lblNavTit.text      = task.taskTitle
    
        lblStartTime.text   = "\(task.startTimeLocal) - \(task.endTimeLocal)"
        lblAddress.text     = task.customerAddress
        
        //renew entites
        let tbvc = self.tabBarController as! TrackTabBarController
        entites = tbvc.entites
        if entites.count > 0 {
            var crew = "Crew: "
            lblCrew.isHidden = false
            for id in task.entityIds {
                for e in entites {
                    if id == e.id {
                        crew = crew + " \(e.name),"
                        break
                    }
                }
            }
            lblCrew.text = String(crew.characters.dropLast(1)) //remove last "," symbol
        } else {
            lblCrew.isHidden = true
        }
        btnETA.layer.borderColor = kColorGreen.cgColor
        
        //status config
        clearButtons()
        cornerButton(button: btnOurWay, color: kColorBlue)
        cornerButton(button: btnTaskStart, color: kColorBlueSecond)
        cornerButton(button: btnTaskComplete, color: kColorGreen)
        cornerButton(button: btnCancel, color: kColorCinnabar)
        cornerButton(button: btnException, color: kColorCinnabarSecond)
        cornerButton(button: btnContact, color: kColorPink)
        getStatuses {
            self.isSendingStatus(value: false)
            for e in self.statuses {
                if let status = StatusName(rawValue: e.type) {
                    self.status = status
                    self.setupStatus()
                }
            }
            self.updateStatusLabelText()
            self.estimateTime {
                SVProgressHUD.popActivity()
                self.setInteration(value: true)
                self.scrollView.es_stopPullToRefresh(completion: true)
                self.scrollView.isUserInteractionEnabled = true
            }
        }
        constraintTableViewHeight.constant = self.statusView.frame.size.height > calculateTableViewFrameHeight() ? self.statusView.frame.size.height : calculateTableViewFrameHeight()
        showStatusesView(currentView: currentView, animated: false)
    }
    
    
    
    func showStatusesView(currentView: CurrentViewName, animated: Bool) {
        if self.currentView == currentView {
            return
        }
        
        let offset = self.statusViewButton.frame.size.width
        var frame = self.indicator.frame
        if currentView == .statuses {
            constraintTableViewHeight.constant = self.statusView.frame.size.height
            tableView.isHidden = true
            statusView.isHidden = false
            statusViewButton.setTitleColor(UIColor.black, for: .normal)
            detailsViewButton.setTitleColor(UIColor.gray, for: .normal)
            frame.origin.x = 0
        } else {
            constraintTableViewHeight.constant = calculateTableViewFrameHeight()
            tableView.isHidden = false
            statusView.isHidden = true
            statusViewButton.setTitleColor(UIColor.gray, for: .normal)
            detailsViewButton.setTitleColor(UIColor.black, for: .normal)
            frame.origin.x = frame.origin.x + offset
        }
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                self.indicator.frame = frame
            })
        } else {
            self.indicator.frame = frame
        }
        self.currentView = currentView
    }
    
    @IBAction func showStatusView(_ sender: Any) {
        showStatusesView(currentView: .statuses, animated: true)
    }
    
    @IBAction func showDetailsView(_ sender: Any) {
        showStatusesView(currentView: .details, animated: true)
    }
    
    //MARK: Estmate time
    func estimateTime(completion:@escaping ()->()) {
        guard let taskID = UserDefaults.standard.value(forKey: kTaskID) as? Int64 else {
            return
        }
        let url = kAPIEstimate.appending("\(taskID)/estimate")
        SVProgressHUD.show()
        setInteration(value: false)
        
        var param = [String:AnyObject]()
        var entity_id : Int64 = -1
        if UserDefaults.standard.value(forKey: "company_owned") as! Bool {
            entity_id = UserDefaults.standard.value(forKey: kEntityID) as! Int64
        } else {
            entity_id = UserDefaults.standard.value(forKey: "default_entity_id") as! Int64
        }
        if let isGPSOn = UserDefaults.standard.value(forKey: kLocationService) as? Bool, isGPSOn == true {
            if let lastLat = UserDefaults.standard.value(forKey: kLastLat) as? Double,
                let lastLon = UserDefaults.standard.value(forKey: kLastLon) as? Double {
                if self.task.entityIds.contains(Int64(entity_id)) {
                    param = ["lat": lastLat as AnyObject, "lng": lastLon as AnyObject]
                }
            }
        }
                
        Alamofire.request(url, method: .get, parameters: param).responseJSON {
            response in
            print("func estimateTime, response: \(response)")
            if let json = response.result.value as? NSDictionary {
                if let time = json[kJSCEstimate] as? String {
                    self.btnETA.setTitle("ETA \(time)", for: UIControlState.normal)
                }
            }
            completion()
        }
    }
    
    func setInteration(value: Bool) {
        btnEstimate.isUserInteractionEnabled = value
        btnSMS.isUserInteractionEnabled = value
        btnBack.isUserInteractionEnabled = value
        btnCall.isUserInteractionEnabled = value
        btnMail.isUserInteractionEnabled = value
        segmentView.isUserInteractionEnabled = value
    }

    //MARK: Event
    @IBAction func clickedBtnBack(_ sender: AnyObject) {
        if let navController = self.navigationController, navController.viewControllers.count >= 2 {
            if let taskListVC = navController.viewControllers[navController.viewControllers.count - 2] as? TaskViewController {
                taskListVC.userComeFromDetailTask = true
            }
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedBtnCall(_ sender: AnyObject) {
        let phone = task.customerPhone
        if let url = NSURL(string: "tel://\(phone)") {
            UIApplication.shared.openURL(url as URL)
        }
    }

    @IBAction func clickedBtnMessage(_ sender: AnyObject) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = kEmptyString
            controller.recipients = [self.task.customerPhone]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    @IBAction func clickedBtnEstimate(_ sender: AnyObject) {
        //open maps.app with address:
        
        let addressEnc = task.customerAddress.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let aurl = NSURL(string: "http://maps.apple.com/?address=" + addressEnc) as! URL
        let gurl = NSURL(string: "comgooglemaps://?q=" + addressEnc) as! URL
        
        if UIApplication.shared.canOpenURL(gurl) && UIApplication.shared.canOpenURL(aurl) {
            //show alert
            let alert = UIAlertController(title: "Choose maps app:", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Google maps", style: UIAlertActionStyle.default, handler: {
                action in
                UIApplication.shared.openURL(gurl)
            }))
            alert.addAction(UIAlertAction(title: "Apple maps", style: UIAlertActionStyle.default, handler: {
                action in
                UIApplication.shared.openURL(aurl)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        } else if UIApplication.shared.canOpenURL(gurl) {
            //open gmaps
            UIApplication.shared.openURL(gurl)
        } else if UIApplication.shared.openURL(aurl) {
            //open apple maps
            UIApplication.shared.openURL(aurl)
        } else {
            //show error
            print("you have no map app!")
        }
    }
    
    @IBAction func clickedBtnMail(_ sender: AnyObject) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showAlert(title: kEmptyString, message: kAlertEmail)
        }
    }

    func calculateTableViewFrameHeight() -> CGFloat {
        guard task.values.count != 0 else {
            return self.statusView.frame.height
        }
        var height : CGFloat = 0
        for index in 0...task.values.count {
            let indexPath = IndexPath(row: index, section: 0)
            let frame : CGRect = tableView.rectForRow(at: indexPath)
            height = height + frame.size.height
        }
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if task.values[indexPath.row].title == "Task details" {
            let cell = tableView.cellForRow(at: indexPath) as! DetailsViewDetailsSectionExpandableCell
            cell.changeState()
            tableView.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if task.values[indexPath.row].title == "Task details" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellExpandable", for: indexPath) as! DetailsViewDetailsSectionExpandableCell
            cell.setupCell(title: task.values[indexPath.row].title, value: task.values[indexPath.row].value)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as!    DetailsViewDetailsSectionCell
            let showLocationButton = (task.values[indexPath.row].title.lowercased().range(of: "address") != nil) ||
                (task.values[indexPath.row].title.lowercased().range(of: "location") != nil) ||
                (task.values[indexPath.row].title.lowercased().range(of: "zip") != nil)
            cell.setupCell(title: task.values[indexPath.row].title, value: task.values[indexPath.row].value, showLocationButton: showLocationButton)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return task.values.count
        
    }
    
//MARK: Status View
        func setupStatus() {
            if self.status == nil {
                return
            }
            switch status!.rawValue {
            case kEnRoute:
                self.status = StatusName.onOurWay
                chageStauts(button: self.btnOurWay, label:lblOnway)
                break
            case kStarted:
                self.status = StatusName.start
                chageStauts(button: self.btnTaskStart, label:lblStart)
                break
            case kNotStarted:
                self.status = StatusName.start
                chageStauts(button: self.btnTaskStart, label:lblStart)
                break
            case kComplete:
                self.status = StatusName.complete
                chageStauts(button: self.btnTaskComplete, label:lblComplete)
                break
            case kCancelled:
                self.status = StatusName.cancel
                chageStauts(button: self.btnCancel, label:lblCancel)
                break
            case kException:
                self.status = StatusName.exception
                chageStauts(button: self.btnException, label:lblException)
                break
            case kCustom:
                self.status = StatusName.custom
                chageStauts(button: self.btnContact, label:lblContact)
                break
            default:
                break
            }
        }
        
        func cornerButton(button : UIButton, color : UIColor) {
            button.layer.cornerRadius = circleSize/2.0
            button.layer.borderColor = color.cgColor
            button.layer.borderWidth = 0.8
            button.clipsToBounds = true
        }
        
        @IBAction func clickedBtnOurway(_ sender: AnyObject) {
            status = StatusName.onOurWay
            chageStauts(button: self.btnOurWay, label: lblOnway)
            sendStauts(type: kEnRoute, time: Date(), extraField: [:]) {
                self.isSendingStatus(value: false)
            }
        }
        
        @IBAction func clickedBtnStart(_ sender: AnyObject) {
            status = StatusName.start
            chageStauts(button: self.btnTaskStart, label: lblStart)
            
            sendStauts(type: kStarted, time: Date(), extraField: [:]) {
                self.isSendingStatus(value: false)
            }
        }
        
        @IBAction func clickedBtnComplete(_ sender: AnyObject) {
            status = StatusName.complete
            chageStauts(button: self.btnTaskComplete, label:lblComplete)
            
            sendStauts(type: kComplete, time: Date(), extraField: [:]) {
                self.isSendingStatus(value: false)
            }
        }
        
        @IBAction func clickedBtnCancel(_ sender: AnyObject) {
            status = StatusName.cancel
            chageStauts(button: self.btnCancel, label: lblCancel)
            
            sendStauts(type: kCancelled, time: Date(), extraField: [:]) {
                self.isSendingStatus(value: false)
            }
        }
        
        @IBAction func clickedBtnException(_ sender: AnyObject) {
            status = StatusName.exception
            chageStauts(button: self.btnException, label: lblException)
            sendStauts(type: kException, time: Date(), extraField: [:]) {
                self.isSendingStatus(value: false)
            }
        }
        
        @IBAction func clickedBtnContact(_ sender: AnyObject) {
            status = StatusName.custom
            chageStauts(button: self.btnContact, label:lblContact)
            /* no need to send status.
            sendStauts(type: kContact, time: Date(), extraField: [:]) {
                self.isSendingStatus(value: false)
            }
            */
            getEmergency()
    }
        
        //MARK: get emergency
        func getEmergency() {
            SVProgressHUD.show()
            var entity_id: Int64 = -1
            if UserDefaults.standard.value(forKey: "company_owned") as! Bool {
                entity_id = UserDefaults.standard.value(forKey: "owned_company_id") as! Int64
            } else {
                if UserDefaults.standard.value(forKey: "owner") != nil {
                    entity_id = UserDefaults.standard.value(forKey: "owner") as! Int64
                }
            }
            let url = kAPIProfile + "/\(entity_id)/emergency"
            Alamofire.request(url, method: .get).responseJSON {
                response in
                print("func getEmergency, response: \(response)")
                if let emer = (response.result.value as? [String:AnyObject])?["emergency"] as? String {
                    self.showPopup(emergency: emer)
                }
                SVProgressHUD.popActivity()
            }
        }
    
    //MARK popup:
    @IBAction func buttonCloseContactView(_ sender: Any) {
        self.viewContact.isHidden = true
        for view in contentView.subviews {
            view.isUserInteractionEnabled = true
        }

    }
    
    
        func showPopup(emergency: String) {
            for view in contentView.subviews {
                view.isUserInteractionEnabled = false
            }
            viewContact.isUserInteractionEnabled = true
            textViewContact.isUserInteractionEnabled = true
            buttonContact.isUserInteractionEnabled = true
            contentView.isUserInteractionEnabled = true
            textViewContact.text = emergency
            self.viewContact.isHidden = false
        }
        
        func chageStauts(button: UIButton, label: UILabel) {
            button.setTitleColor(UIColor.white, for: UIControlState.normal)
            button.backgroundColor = kColorBlue
            label.textColor = UIColor.white
            updateStatusLabelText()
        }
        func imageFromColor(colour: UIColor) -> UIImage {
            let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
            UIGraphicsBeginImageContext(rect.size)
            let context = UIGraphicsGetCurrentContext()
            context!.setFillColor(colour.cgColor)
            context!.fill(rect)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image!
        }
        func clearButtons() {
            lblOnway.textColor = UIColor.black
            lblStart.textColor = UIColor.black
            lblCancel.textColor = UIColor.black
            lblContact.textColor = UIColor.black
            lblComplete.textColor = UIColor.black
            lblException.textColor = UIColor.black
            
            for sub in self.viewCircles.subviews {
                if(type(of: sub) == UIButton.self) {
                    let btn = sub as! UIButton
                    btn.setTitleColor(UIColor.black, for: UIControlState.normal)
                    btn.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1.0)
                }
            }
            //dont touch motes button
            notesButton.setTitle("Notes", for: .normal)
            notesButton.setTitleColor(kColorMeadow, for: .normal)
            notesButton.backgroundColor = UIColor.clear
        }
    
    @IBAction func buttonNotesPressed(_ sender: Any) {
        let notesVC : NotesController = self.storyboard?.instantiateViewController(withIdentifier: "NotesVC") as! NotesController
        notesVC.statuses = self.statuses
        notesVC.taskDetailVC = self
        notesVC.modalPresentationStyle = .overCurrentContext
        self.tabBarController?.present(notesVC, animated: true, completion: nil)
    }
        func updateStatusLabelText() {
            let lastestStatus = "Lastest status: "
            guard let status = self.status?.rawValue else {
                return
            }
            switch status {
            case "":
                lblStatus.text = "\(lastestStatus)\(kTXTNotStart)"
                break
            case "ENROUTE":
                lblStatus.text = "\(lastestStatus)\(kTXTOnOurWay)"
                break
            case "STARTED":
                lblStatus.text = "\(lastestStatus)\(kTXTTaskStarted)"
                break
            case "COMPLETE":
                lblStatus.text = "\(lastestStatus)\(kTXTTaskComplete)"
                break
            case "CANCELLED":
                lblStatus.text = "\(lastestStatus)\(kTXTTaskCancel)"
                break
            case "EXCEPTION":
                lblStatus.text = "\(lastestStatus)\(kTXTException)"
                break
            case "CUSTOM":
                lblStatus.text = "\(lastestStatus)\(kTXTCustom)"
                break
            default:
                break
            }
        }
        
        func sendStauts(type : String, time : Date, extraField : [String:Any], completion:@escaping ()->()) {
            guard let taskID = UserDefaults.standard.value(forKey: kTaskID) as? Int64 else {
                return
            }
            let url = kAPIUpdateStauts.appending("\(taskID)/status/new")
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let dateString = dateFormatter.string(from: time)
            var reporter_id  : Int64 = 0
            var reporter_name = ""
            if UserDefaults.standard.value(forKey: "company_owned") as! Bool {
                reporter_id = UserDefaults.standard.value(forKey: kEntityID) as! Int64
            } else {
                reporter_id = UserDefaults.standard.value(forKey: "default_entity_id") as! Int64
            }
            if let safe = UserDefaults.standard.value(forKey: "reporter_name") as? String {
                reporter_name = safe
            }
            let parameters = ["type" : type,
                              "time" : dateString,
                              "reporter_id" : String(reporter_id),
                              "reporter_name" : reporter_name]
            isSendingStatus(value: true)
            Alamofire.request(url, method:.post, parameters: parameters, encoding: URLEncoding.default).responseJSON {
                response in
                print("func sendStauts, response: \(response)")
                completion()
                //cache: append status:
                let status = Status()
                status.visibleToCustomer = true
                status.reporterId = reporter_id
                status.type = type
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM d h:mm a"
                dateFormatter.timeZone = NSTimeZone.local
                status.timeLocal = dateFormatter.string(from: Date())
                self.statuses.append(status)
            }
        }
    
        func getStatuses(completion:@escaping ()->()) {
            guard let taskID = UserDefaults.standard.value(forKey: kTaskID) as? Int64 else {
                return
            }
            let url = kAPIUpdateStauts.appending("\(taskID)/status")
            isSendingStatus(value: true)
            
            Alamofire.request(url, method:.get, parameters: [:], encoding: URLEncoding.default).responseJSON {
                response in
                print("func getStatuses, response: \(response)")
                if let dict = response.result.value as? [[String:AnyObject]] {
                    self.statuses.removeAll()
                    for elem in dict {
                        let status = Status()
                        if let extra = elem["extra_fields"] as? NSDictionary {
                            if let visible = extra["visible_to_customer"] as? Bool {
                                status.visibleToCustomer = visible
                            }
                            if let files = extra["files"] as? NSArray {
                                for file in files {
                                    if let dict = file as? NSDictionary,
                                        let filename = dict["file_path"] as? String {
                                            status.files.append(filename)
                                    }
                                }
                            }
                            if let notes = extra["notes"] as? String {
                                status.notes = notes
                            }
                        }
                        if let safe = elem["id"] as? String {
                            status.id = safe
                        }
                        if let safe = elem["reporter_id"] as? Int64 {
                            status.reporterId = safe
                        }
                        if let safe = elem["reporter_name"] as? String {
                            status.reporterName = safe
                        }
                        if let safe = elem["time"] as? String {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                            dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!
                            let utcDate = dateFormatter.date(from: safe)!
                            status.timeUTC = utcDate
                            //convert to local
                            dateFormatter.dateFormat = "MMM d h:mm a"
                            dateFormatter.timeZone = NSTimeZone.local
                            status.timeLocal = dateFormatter.string(from: utcDate)
                        }
                        if let safe = elem["type"] as? String {
                            status.type = safe
                        }
                        self.statuses.append(status)
                    }
                    self.statuses.sort {
                        return $0.timeUTC < $1.timeUTC
                    }
                    if self.statuses.count > 0 {
                        if let status = StatusName(rawValue: self.statuses[self.statuses.count - 1].type) {
                            self.status = status
                        }
                    }
                }
                completion()
            }
        }
        
        func isSendingStatus(value : Bool) {
            setUserInteration(value: !value)
            if(value) {
                SVProgressHUD.show()
            }
            else {
                SVProgressHUD.popActivity()
            }
        }
        
        func setUserInteration(value : Bool) {
            btnOurWay.isUserInteractionEnabled = value
            btnTaskStart.isUserInteractionEnabled = value
            btnTaskComplete.isUserInteractionEnabled = value
            btnCancel.isUserInteractionEnabled = value
            btnException.isUserInteractionEnabled = value
            btnContact.isUserInteractionEnabled = value
        }
        
        
}

extension TaskDetailViewController : MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate {
    //SMS delegate
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //Mail delegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        let email = task.cusEmail
        
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([email])
        mailComposerVC.setSubject("PinThatPoint send mail")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
}
