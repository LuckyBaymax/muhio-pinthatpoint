//
//  User.swift
//  PinThatPoint
//
//  Created by Apple on 10/3/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class User: NSObject {
    static let shareInstance = User()
    private override init(){
    }
    
    var userName : String!
    var email : String!
    var userId : Int!
}
