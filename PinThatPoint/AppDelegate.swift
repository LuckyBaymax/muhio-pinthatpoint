//
//  AppDelegate.swift
//  PinThatPoint
//
//  Created by Apple on 10/1/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import GoogleMaps
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let kGmapsAPIKey = "AIzaSyAbyMGa711EfNSIl3XPsPCPs81YGjYABBw"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //Fabric
        Fabric.with([Crashlytics.self])
        //gmap
        GMSServices.provideAPIKey(kGmapsAPIKey)
        //tabbar
        //UITabBar.appearance().barTintColor = kColorMeadow
        //UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.white], for: .selected)
        //UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.lightText], for: .normal)
        let colorNormal = kColorMeadow
        let colorSelected = UIColor.white
        let titleFontAll : UIFont = UIFont(name: "Abel", size: 15.0)!
        
        let attributesNormal = [
            NSForegroundColorAttributeName : colorNormal,
            NSFontAttributeName : titleFontAll
        ]
        
        let attributesSelected = [
            NSForegroundColorAttributeName : colorSelected,
            NSFontAttributeName : titleFontAll
        ]
        
        UITabBarItem.appearance().setTitleTextAttributes(attributesNormal, for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(attributesSelected, for: .selected)
    
        // Override point for customization after application launch.
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyboard.instantiateViewController(withIdentifier: kVCLogin)
        let mainTabBar = storyboard.instantiateViewController(withIdentifier: kVCMainTabBar)
        if let status = UserDefaults.standard.value(forKey: kTXTStatus) as? Int {
            switch status {
            case kTXTLogout:
                self.window?.rootViewController = loginVC
            case kTXTLogin:
                self.window?.rootViewController = mainTabBar
            default: break
            }
            self.window?.makeKeyAndVisible()
        }
        //set up time inteval at first tiem
        if(UserDefaults.standard.value(forKey: kTimeInterval) == nil) {
            UserDefaults.standard.setValue(40, forKey: kTimeInterval)
        }
        
        UserDefaults.standard.setValue(0.0, forKey: kLastLon)
        UserDefaults.standard.setValue(0.0, forKey: kLastLat)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

