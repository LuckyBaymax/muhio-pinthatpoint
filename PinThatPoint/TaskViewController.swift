//
//  TaskViewController.swift
//  PinThatPoint
//
//  Created by Apple on 10/4/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import FSCalendar

class TaskViewController: UIViewController {

    @IBOutlet weak var lblNumTask: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var tbvTask: UITableView!
    @IBOutlet weak var btnForward: UIButton!
    @IBOutlet weak var btnBackward: UIButton!
    @IBOutlet weak var error_dialog_message: UILabel!

    var today = ""

    private let Localformatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = NSTimeZone.local
        formatter.locale = NSLocale.current
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return formatter
    }()
    
    var listTask : [Task]! = [Task]()
    var listTaskTemp : [Task]! = [Task]()
    var currentDay = Date()
    var userComeFromDetailTask = false
    var entites = [Entity]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbvTask.es_addPullToRefresh {
            [weak self] in
            self?.tbvTask.isUserInteractionEnabled = false
            self?.getTasksFromServer(time: (self?.Localformatter.string(from: (self?.currentDay)!))!)
        }
        error_dialog_message.isHidden = true
        tbvTask.delegate = self
        tbvTask.dataSource = self
        configUI()
        getTasksFromServer(time: Localformatter.string(from: currentDay))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //reload tasks, then user click date at calendar
        if let isUserClickDate = UserDefaults.standard.value(forKey: kSelectCalendar) as? Bool,
            isUserClickDate == true,
            let userClikedDate = UserDefaults.standard.value(forKey: kDateSelected) as? Date {
            currentDay = userClikedDate
            UserDefaults.standard.setValue(false, forKey: kSelectCalendar)
            getTasksFromServer(time: Localformatter.string(from: userClikedDate))
        }
        //reload task if user get back from detail view
        print("userComeFromDetailTask: \(userComeFromDetailTask)")
        if userComeFromDetailTask {
            userComeFromDetailTask = false
            getTasksFromServer(time: Localformatter.string(from: currentDay))
        }
        //renew entites
        let tbvc = self.tabBarController as! TrackTabBarController
        entites = tbvc.entites
    }
    
    func logout() {
        SVProgressHUD.show()
        Alamofire.request(kAPILogout, method: .get).responseJSON {
            response in
            if let json = response.result.value {
                print("make logout. response: \(json)")
            }
            SVProgressHUD.popActivity()
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: kVCLogin)
            self.present(loginVC!, animated: true, completion: nil)
            UserDefaults.standard.setValue(kTXTLogout, forKey: kTXTStatus)
        }
        
    }
    
    func getTasksFromServer(time: String) {
        listTask.removeAll()
        refreshCalendarTitle(date: currentDay as Date!)
        getTask(time: time) {
            self.loadDone()
            self.tbvTask.es_stopPullToRefresh(completion: true)
            self.tbvTask.isUserInteractionEnabled = true
        }
    }
    
    func configUI() {
        tbvTask.rowHeight = UITableViewAutomaticDimension
        tbvTask.estimatedRowHeight = 50.0
        tbvTask.separatorColor = kColorGray
    }
    
    func caculateNumTask(numTask : Int!) {
        if(numTask > 1) {
            self.lblNumTask.text = "\(numTask!) tasks"
        }
        else {
            self.lblNumTask.text = "\(numTask!) task"
        }
    }
    
    func loadDone() {
        SVProgressHUD.popActivity()
        self.listTaskTemp = self.listTask
        //sort task on start-time field:
        self.listTaskTemp.sort(by: {t1, t2 in
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            print(t1.startTimeUTC)
            let s1 = dateFormatter.date(from: t1.startTimeUTC)!
            let s2 = dateFormatter.date(from: t2.startTimeUTC)!
                return s1.compare(s2) == ComparisonResult.orderedAscending
            })

        self.tbvTask.reloadData()
        self.caculateNumTask(numTask: self.listTask.count)
        btnForward.isUserInteractionEnabled = true
        btnBackward.isUserInteractionEnabled = true
        NSLog("==========Load Done+++++++")
    }
    
    func refreshCalendarTitle(date : Date!) {
        lblDate.text = self.dateToString(today: date)
        btnBackward.isEnabled = true
        btnForward.isEnabled = true
    }
    
    //MARK: Date time
    func dateToString(today : Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateFormat = "MMM "
        
        let userCalendar = Calendar.current
        let day = userCalendar.component(.day, from: today) as NSNumber
        
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = NSLocale.current
        numberFormatter.numberStyle = .ordinal
        let daySuffix = numberFormatter.string(from: day)
        
        return dateFormatter.string(from: today) + daySuffix!

    }
    
    //MARK: Get task
    func getTask(time: String!, completion:@escaping ()->()) { //time is local, example: "2016-11-28T03:35:30"
        print("getTask func started: time = \(time)")
        SVProgressHUD.show()
        btnForward.isUserInteractionEnabled = false
        btnBackward.isUserInteractionEnabled = false
        var toTime = ""
        var fromTime = ""
        if (time != "") {
            //For Start Date
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.timeZone = NSTimeZone.local
    
            var calendar = NSCalendar.current
            calendar.timeZone = NSTimeZone.local
            let dateAtMidnight = calendar.startOfDay(for: dateFormatter.date(from: time)!)
            //For End Date
            var components = DateComponents()
            components.day = 1
            components.second = -1
            let dateAtEnd = calendar.date(byAdding: components as DateComponents, to: calendar.startOfDay(for: dateFormatter.date(from: time)!))
            
            dateFormatter.timeZone = NSTimeZone.local
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZ"
            fromTime = dateFormatter.string(from: dateAtMidnight)
            toTime = dateFormatter.string(from: dateAtEnd!)
            print("fromTime: \(fromTime)")
            print("toTime: \(toTime)")
            
        }
        //Supported format is YYYY-MM-DDTHH:mm:ssZZ
        var optionalEntity = ""
        if UserDefaults.standard.value(forKey: "company_owned") as! Bool {
            optionalEntity = "&entity_id=\(UserDefaults.standard.value(forKey: kEntityID) as! Int64)"
        }
        
        var url = (time == "") ? (kAPITaks) : ("\(kAPITaks)?from=\(fromTime)&to=\(toTime)" + optionalEntity)
        url = url.replacingOccurrences(of: "+", with: "%2B")
        Alamofire.request(url, method: .get).responseJSON{
            response in
            print("response: \(url):  \(response)")
            if let arrTask = response.result.value as? NSArray {
                for task in arrTask {
                    if let json = task as? NSDictionary {
                        self.listTask.append(Task(dict: json))
                    }
                }
            }
            else {
                //TODO: test 401 status
                //self.logout()
                //return
            }
            completion()
        }
        
    }
    
    //MARK: Event
    @IBAction func showCalendar(_ sender: AnyObject) {
        let calendarVC = self.storyboard?.instantiateViewController(withIdentifier: kVCCalendar) as! CalendarViewController
        //self.present(calendarVC, animated: true, completion: nil)
        self.navigationController?.pushViewController(calendarVC, animated: true)
    }
    
    @IBAction func clickedBtnIDate(_ sender: AnyObject) {
        let calendarVC = self.storyboard?.instantiateViewController(withIdentifier: kVCCalendar) as! CalendarViewController
        self.present(calendarVC, animated: true, completion: nil)
    }
    
    @IBAction func clickedBtnBackward(_ sender: AnyObject) {
        btnForward.isUserInteractionEnabled = false
        btnBackward.isUserInteractionEnabled = false
        let previousDay = currentDay.addingTimeInterval((-1)*24 * 60 * 60) as Date
        let previousDayStr = Localformatter.string(from: previousDay)
        listTask.removeAll()
        getTask(time: previousDayStr) { 
            self.loadDone()
        }
        currentDay = previousDay
        refreshCalendarTitle(date: previousDay)
        
    }
    
    @IBAction func clickedBtnForward(_ sender: AnyObject) {
        btnForward.isUserInteractionEnabled = false
        btnBackward.isUserInteractionEnabled = false
        let nextDay = currentDay.addingTimeInterval((+1)*24 * 60 * 60) as Date
        let nextDayStr = Localformatter.string(from: nextDay)
        listTask.removeAll()
        getTask(time: nextDayStr) {
            self.loadDone()
        }
        currentDay = nextDay
        refreshCalendarTitle(date: nextDay)
    }
   
}

extension TaskViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listTaskTemp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: kCELLTask, for: indexPath) as! TaskCell
        let task = self.listTaskTemp[indexPath.row]
        let time = "\(task.startTimeLocal) - \(task.endTimeLocal)"
    
        cell.lblTime.text = time
        cell.lblTitle.text = task.taskTitle
        cell.lblAddress.text = task.customerAddress
        cell.status(status: task.status)
        if entites.count > 0 {
            cell.lblCrew.isHidden = false
            var crew = "Crew: "
            for id in task.entityIds {
                for e in entites {
                    if id == e.id {
                        crew = crew + " \(e.name),"
                        break
                    }
                }
            }
            cell.lblCrew.text = String(crew.characters.dropLast(1)) //remove last "," symbol
        } else {
            cell.lblCrew.isHidden = true
        }
        return cell
 
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: kVCDetailTask) as! TaskDetailViewController
        detailVC.task = listTaskTemp[indexPath.row]
        UserDefaults.standard.setValue(listTaskTemp[indexPath.row].taskID, forKey: kTaskID)
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

   
}

