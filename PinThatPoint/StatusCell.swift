//
//  statusCell.swift
//  PinThatPoint
//
//  Created by Alexey Kuznetsov on 06/12/2016.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import Kingfisher
import ImageSlideshow


class StatusCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var viewPictures: ImageSlideshow!
    @IBOutlet weak var imagethemCorner: UIImageView!
    @IBOutlet weak var imageMeCorner: UIImageView!
    @IBOutlet weak var contraintViewPicturesHeight: NSLayoutConstraint!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageEye: UIImageView!
    @IBOutlet weak var constraintViewCellLeading: NSLayoutConstraint!
    @IBOutlet weak var constraintViewCellTrainling: NSLayoutConstraint!
    var notesController: NotesController?
    
    func setupCell(isMe: Bool, name: String, text: String, date: String, pictures: [String], visible: Bool, vc: NotesController) {
        self.notesController = vc
        viewPictures.backgroundColor = UIColor.clear
        viewPictures.slideshowInterval = 0
        viewPictures.pageControlPosition = PageControlPosition.insideScrollView
        viewPictures.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        viewPictures.pageControl.pageIndicatorTintColor = UIColor.black
        viewPictures.contentScaleMode = UIViewContentMode.scaleAspectFill
        cellView.layer.cornerRadius = 5.0
        self.labelName.text = name
        self.labelText.text = text
        self.labelDate.text = date
        var kingfisherSource = [InputSource]()
        if pictures.count > 0 {
            contraintViewPicturesHeight.constant = 100.0
            for url in pictures {
                kingfisherSource.append(KingfisherSource(urlString: url)!)
            }
            viewPictures.setImageInputs(kingfisherSource)
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
            viewPictures.addGestureRecognizer(recognizer)
        } else {
            contraintViewPicturesHeight.constant = 0
        }
        cellView.clipsToBounds = true
        imageEye.isHidden = !visible
        if isMe {
            cellView.backgroundColor = UIColor(red: 243.0/255.0, green: 1.0, blue: 236.0/255.0, alpha: 1.0)
            constraintViewCellLeading.constant = 40.0
            constraintViewCellTrainling.constant = 15.0
            imagethemCorner.isHidden = true
            imageMeCorner.isHidden = false
        } else {
            cellView.backgroundColor = UIColor.white
            constraintViewCellLeading.constant = 15.0
            constraintViewCellTrainling.constant = 40.0
            imagethemCorner.isHidden = false
            imageMeCorner.isHidden = true
        }
        
    }
    
    func didTap() {
        viewPictures.presentFullScreenController(from: notesController!)
    }
}

public class KingfisherSource: NSObject, InputSource {
    var url: URL
    var placeholder: UIImage?
    
    public init(url: URL) {
        self.url = url
        super.init()
    }
    
    public init(url: URL, placeholder: UIImage) {
        self.url = url
        self.placeholder = placeholder
        super.init()
    }
    
    public init?(urlString: String) {
        if let validUrl = URL(string: urlString) {
            self.url = validUrl
            super.init()
        } else {
            return nil
        }
    }
    
    @objc public func load(to imageView: UIImageView, with callback: @escaping (UIImage) -> ()) {
        imageView.kf.setImage(with: self.url, placeholder: self.placeholder, options: nil, progressBlock: nil) { (image, _, _, _) in
            if let image = image {
                callback(image)
            }
        }
    }
}

public class ImageSource: NSObject, InputSource {
    var image: UIImage!
    
    public init(image: UIImage) {
        self.image = image
    }
    
    public func load(to imageView: UIImageView, with callback: @escaping (UIImage) -> ()) {
        imageView.image = image
        callback(image)
    }
}

