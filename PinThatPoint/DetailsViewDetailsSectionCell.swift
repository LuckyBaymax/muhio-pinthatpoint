//
//  DetailViewCell.swift
//  PinThatPoint
//
//  Created by Alexey Kuznetsov on 20/11/2016.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

let kMaximumLinesToShow = 3

class DetailsViewDetailsSectionExpandableCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    @IBOutlet weak var labelClickToExpand: UILabel!
    
    @IBOutlet weak var constraintLabelClickToExpandHeight: NSLayoutConstraint!
    var isExpanded = false
    
    func setupCell(title: String, value: String) {
        labelTitle.text = title
        labelValue.text = value
        setupUI()
    }
    
    func changeState() {
        isExpanded = !isExpanded
        setupUI()
    }
    
    func setupUI() {
        if isExpanded == true {
            labelValue.numberOfLines = 0
            labelClickToExpand.isHidden = true
            constraintLabelClickToExpandHeight.constant = 0
        } else {
            labelValue.numberOfLines = kMaximumLinesToShow
            labelClickToExpand.isHidden = false
            constraintLabelClickToExpandHeight.constant = 17
        }
        labelValue.sizeToFit()
    }
    
}

class DetailsViewDetailsSectionCell : UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    @IBOutlet weak var buttonLocation: UIButton!
    
    @IBOutlet weak var buttonLocationWidthConstraint: NSLayoutConstraint!
    func setupCell(title: String, value: String, showLocationButton: Bool) {
        self.labelTitle.text = title
        self.labelValue.text = value
        if showLocationButton {
            self.buttonLocation.addTarget(self, action: #selector(openAddress), for: .touchUpInside)
            self.buttonLocationWidthConstraint.constant = 30.0
        } else {
            self.buttonLocationWidthConstraint.constant = 0
            self.buttonLocation.removeTarget(nil, action: nil, for: .allEvents)
        }
        self.layoutIfNeeded()
    }
    
    func openAddress() {
        let addressEnc = self.labelValue.text!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        let aurl = NSURL(string: "http://maps.apple.com/?address=" + addressEnc) as! URL
        let gurl = NSURL(string: "comgooglemaps://?q=" + addressEnc) as! URL
        
        if UIApplication.shared.canOpenURL(gurl) && UIApplication.shared.canOpenURL(aurl) {
            //show alert
            let alert = UIAlertController(title: "Choose maps app:", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Google maps", style: UIAlertActionStyle.default, handler: {
                action in
                UIApplication.shared.openURL(gurl)
            }))
            alert.addAction(UIAlertAction(title: "Apple maps", style: UIAlertActionStyle.default, handler: {
                action in
                UIApplication.shared.openURL(aurl)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        } else if UIApplication.shared.canOpenURL(gurl) {
            //open gmaps
            UIApplication.shared.openURL(gurl)
        } else if UIApplication.shared.openURL(aurl) {
            //open apple maps
            UIApplication.shared.openURL(aurl)
        } else {
            //show error
            print("you have no map app!")
        }
    }
    
}
