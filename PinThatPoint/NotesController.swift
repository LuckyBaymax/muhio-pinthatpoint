//
//  NotesController.swift
//  PinThatPoint
//
//  Created by Alexey Kuznetsov on 23/11/2016.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import AssetsLibrary
import AVFoundation
import Photos
import Kingfisher
import ImageSlideshow
import BSImagePicker
import Photos

class NotesController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource {
    //Mark: outlets
    @IBOutlet weak var labelEmptyStatusPlaceholder: UILabel!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var labelPlaceholder: UILabel!
    @IBOutlet weak var buttonAttach: UIButton!
    @IBOutlet weak var buttonPicAttch: UIButton!
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var visibleSwitch: UISwitch!
    @IBOutlet weak var actionsView: UIView!
    @IBOutlet weak var labelHint: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewPreview: ImageSlideshow!
    //@IBOutlet weak var labelCameraCount: UILabel!
    
    //Mark: constraints
    @IBOutlet weak var constraintPreviewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTopViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintBottomViewHeight: NSLayoutConstraint!
    // Mark: Vars
    var imagePicker = UIImagePickerController()
    var imageLibraryPicker = BSImagePickerViewController()
    var imagesFromCamera = [UIImage]()
    var imagesFromLibrary = [UIImage]()
    var files = [[String:String]]()
    var statuses = [Status]()
    var entityID: Int64 = 0
    var taskDetailVC: TaskDetailViewController?
    
    //MARK: Startups
    override func viewDidLoad() {
        super.viewDidLoad()
        //placeholder
        hidePlaceholder(hide: false)
        labelPlaceholder.isUserInteractionEnabled = true
        //set properly views size
        let screenHeight = UIScreen.main.bounds.height
        let topEdge: CGFloat = 30.0 + 40.0 //top bar edge + top bar
        let bottomEdge: CGFloat = 30.0 + 10.0 //scrollView bottom edge + bottomView bottom edge
        let viewHeight = (screenHeight - topEdge - bottomEdge)/2
        constraintTopViewHeight.constant = viewHeight
        constraintBottomViewHeight.constant = viewHeight
        constraintPreviewHeight.constant = 0
        
        if UserDefaults.standard.value(forKey: "company_owned") as! Bool {
            self.entityID = UserDefaults.standard.value(forKey: kEntityID) as! Int64
        } else {
            self.entityID = UserDefaults.standard.value(forKey: "default_entity_id") as! Int64
        }
        
        setupUI()
        setupPreview()
        setupKeyboard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func resetUI() {
        visibleSwitch.setOn(false, animated: true)
        imagesFromCamera.removeAll()
        noteTextView.text = ""
        imagesFromLibrary.removeAll()
        hidePlaceholder(hide: false)
        setupPhotoButtons()
        setupUI()
    }
    
    //MARK: image picker
    func setupAndShowImagePicker(sourceType: UIImagePickerControllerSourceType) {
        //check access
        if sourceType == .camera {
            let cameraMediaType = AVMediaTypeVideo
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
            
            switch cameraAuthorizationStatus {
            case .denied, .restricted:
                showAlertResourceIsUnavailable(sourceType: sourceType)
                return
            case .authorized:
                break
            case .notDetermined:
                // Prompting user for the permission to use the camera.
                AVCaptureDevice.requestAccess(forMediaType: cameraMediaType) { granted in
                    if granted == false {return}
                }
            }
        } else {
            let status = PHPhotoLibrary.authorizationStatus()
            switch status {
            case .denied, .restricted:
                showAlertResourceIsUnavailable(sourceType: sourceType)
                return
            case .authorized:
                break
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization({ (status) in
                    if status != PHAuthorizationStatus.authorized {return}
                })
            }
        }
        //try to open resource
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            if sourceType == .camera {
                imagePicker.delegate = self
                imagePicker.sourceType = sourceType;
                imagePicker.allowsEditing = false
                present(imagePicker, animated: true, completion: nil)
            } else {
                presentMultipleImagePicker()
            }
        } else {
            showAlertResourceIsUnavailable(sourceType: sourceType)
        }
    }
    
    func presentMultipleImagePicker() {
        imageLibraryPicker.maxNumberOfSelections = 3
        imageLibraryPicker.takePhotos = false
        bs_presentImagePickerController(imageLibraryPicker, animated: true,
                                        select: { (asset: PHAsset) -> Void in
                                            print("Selected: \(asset)")
        }, deselect: { (asset: PHAsset) -> Void in
            print("Deselected: \(asset)")
        }, cancel: { (assets: [PHAsset]) -> Void in
            self.updateImagesFromLibrary(assets: assets)
            print("Cancel: \(assets)")
        }, finish: { (assets: [PHAsset]) -> Void in
            self.updateImagesFromLibrary(assets: assets)
            print("Finish: \(assets)")
        }, completion: nil)
    }
    
    func updateImagesFromLibrary(assets: [PHAsset]) {
        self.imagesFromLibrary.removeAll()
        if assets.count > 0 {
            for asset in assets {
                // this is very slow. TODO: refactor
                let manager = PHImageManager.default()
                let options = PHImageRequestOptions()
                options.version = .original
                manager.requestImageData(for: asset, options: options) { data, _, _, _ in
                    if let data = data {
                        self.imagesFromLibrary.append(UIImage(data: data)!)
                        self.setupPhotoButtons()
                    }
                }
            }
        }
    }
    /* crashes TODO: fix this
    func saveImageAndTakePHAsset(image: UIImage) -> PHAsset? {
        var localId:String?
        let imageManager = PHPhotoLibrary.shared()
        var imageAsset : PHAsset?
        imageManager.performChanges({ () -> Void in
            let request = PHAssetChangeRequest.creationRequestForAsset(from: image)
            localId = request.placeholderForCreatedAsset?.localIdentifier
        }, completionHandler: { (success, error) -> Void in
            DispatchQueue.main.sync(execute: { () -> Void in
                if let localId = localId {
                    let result = PHAsset.fetchAssets(withLocalIdentifiers: [localId], options: nil)
                    let assets = result.objects(at: NSIndexSet(indexesIn: NSRange(location: 0, length: result.count)) as IndexSet)
                    if let asset = assets.first {
                        imageAsset = asset
                    }
                }
            })
        })
        return imageAsset
    }*/
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imagesFromCamera.append(pickedImage)
            setupPhotoButtons()
        }
        dismiss(animated: true, completion: nil)
    }
    func showAlertResourceIsUnavailable(sourceType: UIImagePickerControllerSourceType) {
        let alertTitle = sourceType == .camera ? "Camera is not available, it is already used or app doesnt have access to it. Open settings to check access rights?" : "Photo library is not available, bacause it is empty or app doesnt have access to it. Open settings to check access rights?"
        let alert = UIAlertController(title: alertTitle, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Open settings", style: .default, handler: { (action) in
            UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString) as! URL)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK: preview
    func setupPreview() {
        viewPreview.slideshowInterval = 2.0
        viewPreview.pageControlPosition = PageControlPosition.insideScrollView
        viewPreview.pageControl.currentPageIndicatorTintColor = UIColor.black
        viewPreview.pageControl.pageIndicatorTintColor = UIColor.white
        viewPreview.pageControl.hidesForSinglePage = true
        viewPreview.contentScaleMode = UIViewContentMode.scaleAspectFill
        
        let recognizerPreviewTap = UITapGestureRecognizer(target: self, action: #selector(didPreviewTap))
        viewPreview.addGestureRecognizer(recognizerPreviewTap)
    }
    //MARK: setup UI
    func setupUI() {
        if statuses.count > 0 {
            labelEmptyStatusPlaceholder.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
            tableView.tableViewScrollToBottom(animated: true)
        } else {
            labelEmptyStatusPlaceholder.isHidden = false
            tableView.isHidden = true
        }
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 50.0
        //make green borders
        actionsView.layer.borderWidth = 1.0
        actionsView.layer.borderColor = kColorMeadow.cgColor
        bottomView.layer.borderWidth = 1.0
        bottomView.layer.borderColor = kColorMeadow.cgColor
        //round save button
        buttonSave.layer.cornerRadius = 2.0
        buttonSave.clipsToBounds = true
        //photobuttons template color
        setupPhotoButtons()
        let labelPlaceholderTap = UITapGestureRecognizer(target: self, action: #selector(placeholderWasTapped))
        labelPlaceholder.addGestureRecognizer(labelPlaceholderTap)
        labelPlaceholderTap.delegate = self
    }
    
    func placeholderWasTapped(sender: UITapGestureRecognizer) {
        hidePlaceholder(hide: true)
        noteTextView.becomeFirstResponder()
    }
    
    func hidePlaceholder(hide: Bool) {
        labelPlaceholder.isHidden = hide
    }
    
    func setupPhotoButtons() {
        buttonAttach.tintColor = (imagesFromLibrary.count == 0) ? UIColor.lightGray : kColorMeadow
        buttonPicAttch.tintColor = (imagesFromCamera.count == 0) ? UIColor.lightGray : kColorMeadow
        if (imagesFromCamera.count == 0) && (imagesFromLibrary.count == 0) {
            constraintPreviewHeight.constant = 0
        } else {
            constraintPreviewHeight.constant = 100
            var imageSource = [InputSource]()
            for image in imagesFromCamera {
                imageSource.append(ImageSource(image: image))
            }
            for image in imagesFromLibrary {
                imageSource.append(ImageSource(image: image))
            }
            viewPreview.setImageInputs(imageSource)
        }
    }
    
    func didPreviewTap() {
        viewPreview.presentFullScreenController(from: self)
    }
    
    //MARK: button Cancel
    @IBAction func buttonCancelWasPressed(_ sender: Any) {
        closeNote()
    }
    
    //MARK: switch
    @IBAction func switchVisibleChangeValue(_ sender: Any) {
        labelHint.textColor = visibleSwitch.isOn ? UIColor.black : UIColor.gray
    }
    
    //MARK: button Save
    @IBAction func buttonSavePressed(_ sender: Any) {
        noteTextView.resignFirstResponder()
        saveNote()
    }
    
    //MARK: button Attach
    @IBAction func buttonAttachPressed(_ sender: Any) {
        setupAndShowImagePicker(sourceType: .photoLibrary)
    }
    
    //MARK: button Attach image
    @IBAction func buttonPicAttachPressed(_ sender: Any) {
        if imagesFromCamera.count > 0 {
            let alertTitle = "Take new photo or delete all camera photos?"
            let alert = UIAlertController(title: alertTitle, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Take new photo", style: .default, handler: { (action) in
                self.setupAndShowImagePicker(sourceType: .camera)
            }))
            alert.addAction(UIAlertAction(title: "Delete photos attached from camera", style: .destructive, handler: {(action) in
                self.imagesFromCamera.removeAll()
                self.setupPhotoButtons()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            setupAndShowImagePicker(sourceType: .camera)
        }
    }
    
    //MARK: keyboard work
    func setupKeyboard() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            scrollView.contentInset = UIEdgeInsets.zero
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        scrollView.scrollIndicatorInsets = scrollView.contentInset
        var frame = noteTextView.frame
        frame.size.height = frame.size.height + 70 //add height of autowords of keyboard + some space
        scrollView.scrollRectToVisible(frame, animated: true)
    }
    
    //MARK: textview delegate
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        hidePlaceholder(hide: true)
        return true
    }
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if noteTextView.text == "" {
            hidePlaceholder(hide: false)
        }
        return true
    }
    
    //MARK: tableview delegate
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "statusCell", for: indexPath) as! StatusCell
        let status = statuses[indexPath.row]
        let text = status.type == "CUSTOM" ? status.notes : status.type.lowercased().capitalized
        let isMe = status.reporterId == entityID
        let name = isMe ? "Me" : status.reporterName
        let visible = status.visibleToCustomer
        cell.setupCell(isMe: isMe, name: name, text: text, date: status.timeLocal, pictures: status.files, visible: visible, vc: self)
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statuses.count
        
    }
    
    //MARK: Network work
    func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        if JSONSerialization.isValidJSONObject(value) {
            do {
                let data = try JSONSerialization.data(withJSONObject: value, options: options)
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    print("JSONStringify is succes, result: \(string)")
                    return string as String
                }
            } catch {
                print("JSONStringify error!")
            }
        }
        return ""
    }
    
    func closeNote() {
        view.endEditing(true)
        taskDetailVC?.statuses = self.statuses
        self.dismiss(animated: true, completion: nil)
    }
    
    func saveNote() {
        //if we have files, upload them and get { file_id: xxxxxx, file_path:”.........”}. Then files upload is done, send other fields.
        uploadFiles ()
    }
    
    func uploadFiles() {
        //clean files
        self.files.removeAll()
        //setup vars
        guard let taskID = UserDefaults.standard.value(forKey: kTaskID) as? Int else {
            return
        }
        let uploadUrl = "https://www.arrivy.com/api/tasks/\(taskID)/status/file/upload/url"
        //add images to array
        var images = [Data]()
        if imagesFromCamera.count > 0 {
            for image in imagesFromCamera {
                images.append(UIImageJPEGRepresentation(image, 0.8)!)
            }
        }
        if imagesFromLibrary.count > 0 {
            for image in imagesFromLibrary {
                images.append(UIImageJPEGRepresentation(image, 0.8)!)
            }
        }
        //start to upload files
        let requestGroup =  DispatchGroup()
        //Need as many of these statements as we have files:
        SVProgressHUD.show()
        for image in images {
            requestGroup.enter()
            //get upload url and upload file
            Alamofire.request(uploadUrl, method:.get).responseJSON {
                response in
                if let json = response.result.value as? NSDictionary {
                    if let upload_url = json["upload_url"] as? String {
                        // upload file
                        Alamofire.upload(multipartFormData: { (multipartFormData) in
                            multipartFormData.append(image, withName: "file-0", fileName: "picture", mimeType: "image/jpeg")
                        }, to: upload_url, encodingCompletion: {
                            encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    print("Files: \(response.result.value)")
                                    if let json = response.result.value as? NSDictionary {
                                        if let file_id = json["file_id"] as? Int64,
                                            let file_path = json["file_path"] as? String {
                                            self.files.append(["file_id":String(file_id), "file_path":file_path])
                                            print("got \(file_id) : \(file_path)")
                                        }
                                    }
                                    requestGroup.leave()
                                }
                            case .failure(let encodingError):
                                self.showAlert(title: "Saving note failed! Please, try again.", message: "")
                                print(encodingError)
                                requestGroup.leave()
                            }
                        })
                    }
                } else {
                    self.showAlert(title: "Saving note failed! Please, try again.", message: "")
                }
            }
        }
        
        //This only gets executed once all the above are done
        requestGroup.notify(queue: DispatchQueue.main, execute: {
            SVProgressHUD.popActivity()
            print("DEBUG: all Done")
            let url = kAPIUpdateStauts + "\(taskID)/status/new"
            let note = self.noteTextView.text!
            let visible_to_customer = self.visibleSwitch.isOn
            let type = "CUSTOM"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
            let time = dateFormatter.string(from: Date())
            print("send note time: \(time)")
            var reporter_name = ""
            if let safe = UserDefaults.standard.value(forKey: "reporter_name") as? String {
                reporter_name = safe
            }
            //json
            let extra_fields = ["visible_to_customer":visible_to_customer,
                                "notes": note,
                                "files":self.files] as [String : Any]
            let params = ["type":type,
                          "time":time,
                          "reporter_id":String(self.entityID),
                          "reporter_name":reporter_name,
                          "extra_fields":self.JSONStringify(value: extra_fields as AnyObject)]
            //send
            SVProgressHUD.show()
            Alamofire.request(url, method:.post, parameters: params, encoding: URLEncoding.default).responseJSON {
                response in
                SVProgressHUD.popActivity()
                print("func saveNote, response: \(response)")
                switch response.result {
                case .failure(let error):
                    print(error)
                    self.showAlert(title: "Saving note failed! Please, try again.", message: "")
                case .success(let responseObject):
                    print("sendLocation is success:  \(responseObject)")
                    dateFormatter.dateFormat = "MMM d h:mm a"
                    dateFormatter.timeZone = NSTimeZone.local
                    let timeLocal = dateFormatter.string(from: Date())
                    let newStatus = Status(id: "", type: type, timeLocal: timeLocal, reporterName: reporter_name, reporterId: self.entityID)
                    newStatus.timeLocal = timeLocal
                    newStatus.visibleToCustomer = self.visibleSwitch.isOn
                    newStatus.notes = note
                    for file in self.files {
                        if let fileLink = file["file_path"] {
                            newStatus.files.append(fileLink)
                        }
                    }
                    self.statuses.append(newStatus)
                    self.resetUI()
                }
            }
        })
        
    }
}

extension UITableView {
    
    func tableViewScrollToBottom(animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(250)) {
            
            let numberOfSections = self.numberOfSections
            let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
            if numberOfRows > 0 {
                let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                self.scrollToRow(at: indexPath, at: UITableViewScrollPosition.bottom, animated: animated)
            }
        }
    }
}
