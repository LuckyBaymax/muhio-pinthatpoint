//
//  TaskCell.swift
//  PinThatPoint
//
//  Created by Apple on 10/6/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class TaskCell: UITableViewCell {

    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnStatus: UIButton!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblCrew: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.contentView.layer.borderColor = kColorGrayBorder.cgColor
//        self.contentView.layer.borderWidth = 2.0
//        self.contentView.clipsToBounds = true
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.layoutIfNeeded()
//        let f = contentView.frame
//        let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(5, 0, 5, 0))
//        contentView.frame = fr
    }

    func status(status : String!) {
//        self.btnStatus.layer.cornerRadius = 13.0
//        self.btnStatus.layer.borderWidth = 1.0
        
        if(status! == kTXTComplete) {
            self.btnStatus.setTitle(kTXTCompleted, for: UIControlState.normal)
            self.btnStatus.setTitleColor(kColorMeadow, for: UIControlState.normal)
        }
        else if(status! == kEnRoute) {
            self.btnStatus.setTitle(kEnRoute, for: UIControlState.normal)
            self.btnStatus.setTitleColor(kColorMeadow, for: UIControlState.normal)
        }
        else if(status! == kNotStarted) {
            self.btnStatus.setTitle("NOT STARTED", for: UIControlState.normal)
            self.btnStatus.setTitleColor(kColorGrayBorder, for: UIControlState.normal)
        }
        else if(status! == kStarted) {
            self.btnStatus.setTitle(kStarted, for: UIControlState.normal)
            self.btnStatus.setTitleColor(kColorMeadow, for: UIControlState.normal)
        }
        else if(status! == kCancelled) {
            self.btnStatus.setTitle(kTXTCancelled, for: UIControlState.normal)
            self.btnStatus.setTitleColor(kColorCinnabar, for: UIControlState.normal)
        }
        else if(status! == kException) {
            self.btnStatus.setTitle(kException, for: UIControlState.normal)
            self.btnStatus.setTitleColor(kColorCinnabar, for: UIControlState.normal)
        }
        else if(status! == kCustom) {
            self.btnStatus.setTitle(kCustom, for: UIControlState.normal)
            self.btnStatus.setTitleColor(kColorCinnabar, for: UIControlState.normal)
        }
    }

}
