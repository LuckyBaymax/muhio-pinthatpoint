//
//  RegisterViewController.swift
//  PinThatPoint
//
//  Created by Apple on 10/4/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtFullname: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnRegister: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configUI()
    }
    
    func configUI() {
        txtEmail.layer.sublayerTransform = CATransform3DMakeTranslation(5.0, 0.0, 0.0)
        txtPassword.layer.sublayerTransform = CATransform3DMakeTranslation(5.0, 0.0, 0.0)
        txtFullname.layer.sublayerTransform = CATransform3DMakeTranslation(5.0, 0.0, 0.0)
        self.hideKeyboardWhenTappedAround()
    }
    
    //MARK: Register
    func register(email : String, password : String, fullname : String, completion:@escaping ()->()) {
        SVProgressHUD.show()
        btnRegister.isUserInteractionEnabled = false
        let parameters = [kJSCEmail : email,
                          kJSCPassword : password,
                          kJSCFullname : fullname]
        DispatchQueue.global().async {
            Alamofire.request(kAPIRegister, method: .post, parameters: parameters, encoding: URLEncoding.default).responseJSON {
                response in
                if let json = response.result.value {
                    print(json)
                }
                completion()
            }
        }
    }
    
    //MARK: Validate
    func validEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    //MARK: Event
    @IBAction func clickedBtnRegister(_ sender: AnyObject) {
        guard let email = txtEmail.text, let password = txtPassword.text, let fullname = txtFullname.text else {
            return
        }
        //validate
        if(email == kEmptyString || password == kEmptyString || fullname == kEmptyString) {
            self.showAlert(title: kEmptyString, message: kAlertTextEmpty)
            return
        }
        if(!validEmail(testStr: email)) {
            self.showAlert(title: kEmptyString, message: kAlertEmail)
            return
        }
        //register
        register(email: email, password: password, fullname: fullname) { 
            SVProgressHUD.popActivity()
            self.dismiss(animated: true, completion: nil)
            self.btnRegister.isUserInteractionEnabled = true
        }
    }
    @IBAction func clickedBtnBack(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }

}
