//
//  TimeView.swift
//  PinThatPoint
//
//  Created by Apple on 11/11/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

protocol TimeViewDelegate {
    func didCancel()
    func didDone(selected : Int)
}

class TimeView: UIView , UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var pickerTime: UIPickerView!
    var dataSource : [String]!
    var currentSelect = 0
    var delegate : TimeViewDelegate!
    
    @IBAction func clickedBtnCancel(_ sender: AnyObject) {
        if(delegate != nil) {
            delegate.didCancel()
        }
    }
    @IBAction func clickedBtnDone(_ sender: AnyObject) {
        if(delegate != nil) {
            delegate.didDone(selected: self.currentSelect)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        pickerTime.delegate = self
        pickerTime.dataSource = self
    }
    
    static func createViewWithData(data : [String]) -> TimeView? {
        if let view = Bundle.main.loadNibNamed("TimeView", owner: self, options: nil)? [0] as? TimeView {
            view.dataSource = data
            return view
        }
        return nil
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentSelect = row
    }


}
