//
//  TrackViewController.swift
//  PinThatPoint
//
//  Created by Apple on 10/1/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import AlamofireImage
import ESPullToRefresh
import GoogleMaps

class Entity {
    var id: Int64 = 0
    var name = ""
    var lat = 0.0
    var lon = 0.0
    var time = Date()
}

class TrackViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet weak var imvAvatar: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblReportLocation: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var messabeBarView: UIView!
    @IBOutlet weak var switchLocation: UISwitch!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var gMapView: GMSMapView!
    
    lazy var geocoder = CLGeocoder()
    var isBackgroundMode = false
    var deferringUpdates = false
    var userLocation = CLLocation(latitude: 0.0, longitude: 0.0)
    var locationManager : CLLocationManager!
    var refreshLocationInteval: Double = 40
    var refreshMapInterval: Double = 45
    var refreshMapTimer = Timer()
    var refreshLocationTimer = Timer()
    var entites = [Entity]()
    var entityID = ""
    
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = NSTimeZone.local
        formatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZZ"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //gmaps
        if let interval = UserDefaults.standard.value(forKey: kTimeInterval) as? Double,
            interval != refreshLocationInteval {
            print("viewDidLoad. refreshLocationInteval was changed: \(interval)")
            self.refreshLocationInteval = interval
        }
        refreshLocationTimer.invalidate()
        configUI()
        configMap()
        setupNotifications()
        startTimer(interval: refreshLocationInteval)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Are we in company acccount ?
        if !(UserDefaults.standard.value(forKey: "company_owned") as! Bool) {
            refreshMapTimer.invalidate()
            refreshMapTimer = Timer.scheduledTimer(timeInterval: refreshMapInterval, target: self, selector: #selector(self.setupEntity), userInfo: nil, repeats: true)
            print("map timer started")
        }
        (self.tabBarController as! TrackTabBarController).setColors()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("map timer stopped")
        refreshMapTimer.invalidate()
    }
    
    func configMap() {
        gMapView.isHidden = UserDefaults.standard.value(forKey: "company_owned") as! Bool
        gMapView.isIndoorEnabled = true
        gMapView.mapType = kGMSTypeNormal
        gMapView.accessibilityElementsHidden = false
        gMapView.isMyLocationEnabled = true
    }
    
    //TODO: refactor this with GCD
    func startTimer(interval: Double){
        refreshLocationTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(self.sendCoordinatesToServer), userInfo: nil, repeats: true)
    }
    
    func getPlaceName() {
        var entity: Int64 = -1
        if UserDefaults.standard.value(forKey: "company_owned") as! Bool {
            entity = UserDefaults.standard.value(forKey: kEntityID) as! Int64
        } else {
            entity = UserDefaults.standard.value(forKey: "default_entity_id") as! Int64
        }
        let lon = self.userLocation.coordinate.longitude
        let lat = self.userLocation.coordinate.latitude
        let time = self.userLocation.timestamp
        let city = ""
        let street = ""
        let country = ""
        
        let meta = ["accuray":self.userLocation.horizontalAccuracy,
                            "speed": self.userLocation.speed,
                            "course":self.userLocation.course] as [String : Any]
        
        self.sendLocation(entity: entity, lat: lat, lon: lon, city: city, street: street, country: country, time:time, meta: meta )
    }
    
    //MARK: send location
    func sendLocation(entity : Int64, lat : Double, lon : Double, city : String, street : String, country : String, time: Date, meta : [String:Any]) {
        //send
        
        let parameters = [[kJSCEntity:entity,
                           kJSCLatitude:lat,
                           kJSCLongtitude:lon,
                           kJSCTime:formatter.string(from: time),
                           kJSCCity:city,
                           kJSCStreet:street,
                           kJSCCountry:country,
                           kJSCMeta:self.JSONStringify(value: meta as AnyObject)]] as Array<[String : Any]>
        

        let url = NSURL(string: kAPISendLocation)
        var request = URLRequest(url: url as! URL)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters)
        
        Alamofire.request(request)
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    print(error)
                    if let data = response.data, let responseString = String(data: data, encoding: .utf8) {
                        print("sendLocation is fails, response: \(responseString)")
                    }
                case .success(let responseObject):
                    print("sendLocation is success:  \(responseObject)")
                }
        }
    }
    
    //MARK: Network work
    func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization.WritingOptions(rawValue: 0)
        if JSONSerialization.isValidJSONObject(value) {
            do {
                let data = try JSONSerialization.data(withJSONObject: value, options: options)
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    print("JSONStringify is succes, result: \(string)")
                    return string as String
                }
            } catch {
                print("JSONStringify error!")
            }
        }
        return ""
    }
    
    
    func sendCoordinatesToServer() {
        //send to server:
        self.getPlaceName()
    }
    
    //MARK: Location
    func initLocationManager() {
        // Create the manager object
        self.locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestAlwaysAuthorization()
        // This is the most important property to set for the manager. It ultimately determines how the manager will
        // attempt to acquire location and thus, the amount of power that will be consumed.
        //TODO: test this to get less power eating
        //locationManager.desiredAccuracy = 45
        //locationManager.distanceFilter = 100
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.distanceFilter = kCLDistanceFilterNone
        // Once configured, the location manager must be "started".
        locationManager.startUpdatingLocation()
    }
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(appWillResignActive), name: .UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshLocationIntevalChanged), name: Notification.Name("refreshLocationIntevalChanged"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(logout), name: Notification.Name("logout"), object: nil)
    }
    
    func appWillResignActive() {
        isBackgroundMode = false
        if locationManager != nil {
            locationManager.stopUpdatingLocation()
            locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
            locationManager.distanceFilter = kCLDistanceFilterNone
            locationManager.pausesLocationUpdatesAutomatically = false
            locationManager.activityType = CLActivityType.automotiveNavigation
            locationManager.startUpdatingLocation()
        }
    }
    
    func appDidBecomeActive() {
        isBackgroundMode = false
    }
    
    func refreshLocationIntevalChanged() {
        if let interval = UserDefaults.standard.value(forKey: kTimeInterval) as? Double {
            print("refreshLocationInteval was changed: \(interval)")
            self.refreshLocationInteval = interval
            self.refreshLocationTimer.invalidate()
            startTimer(interval: interval)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //refresh
        if let interval = UserDefaults.standard.value(forKey: kTimeInterval) as? Double {
            refreshLocationInteval = interval
        }
        
        var pickedLocation: CLLocation? = nil
        
        for i in (0 ... locations.count - 1).reversed() {
            print(locations[i].horizontalAccuracy)
            if(locations[i].horizontalAccuracy > 0.0 && locations[i].horizontalAccuracy < 96.0) {
                pickedLocation = locations[i];
                break;
            }
        }
        
        
        if(pickedLocation != nil) {
            //store data
            if let newLocation = pickedLocation {
                self.userLocation = CLLocation(coordinate: newLocation.coordinate, altitude: newLocation.altitude, horizontalAccuracy:newLocation.horizontalAccuracy, verticalAccuracy: newLocation.verticalAccuracy, course: newLocation.course, speed: newLocation.speed, timestamp: newLocation.timestamp)
                print("new user location from GPS \(newLocation.coordinate.latitude):\(newLocation.coordinate.longitude)")
                
                updateCachedLocationInApp()
            }
            //tell the centralManager that you want to deferred this updatedLocation
            if (isBackgroundMode && !deferringUpdates) {
                deferringUpdates = true
                if locationManager != nil { locationManager.allowDeferredLocationUpdates(untilTraveled: CLLocationDistanceMax, timeout: refreshLocationInteval)
                }
            }
        }
    }
    
    func updateCachedLocationInApp() {
        /*if let lastLat = UserDefaults.standard.value(forKey: kLastLat) as? Double,
            let lastLon = UserDefaults.standard.value(forKey: kLastLon) as? Double,
            userLocation.coordinate.latitude != lastLat,
            userLocation.coordinate.longitude != lastLon {
            //store new coordinates
            print("\(Date()) New place. \(self.userLocation.coordinate.latitude):\(self.userLocation.coordinate.longitude)")
        } else {
            print("\(Date()) You didnt move. \(self.userLocation.coordinate.latitude):\(self.userLocation.coordinate.longitude)")
        }*/
        UserDefaults.standard.set(userLocation.coordinate.longitude, forKey: kLastLon)
        UserDefaults.standard.set(userLocation.coordinate.latitude, forKey: kLastLat)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        deferringUpdates = false
        print("Error while updating location " + error.localizedDescription)
    }
    
    //MARK: setupEntity
    func setupEntity() {
        var url = "\(kAPTrack)"
        if UserDefaults.standard.value(forKey: "company_owned") as! Bool {
            url = url + "/\(UserDefaults.standard.value(forKey: kEntityID) as! Int64)"
        }
        Alamofire.request(url, method: .get).responseJSON{
            response in
            print("setupEntity. response: \(url):  \(response)")
            print("setupEntity. result: \(response.result.value)")
            if (UserDefaults.standard.value(forKey: "company_owned") as! Bool), let json = response.result.value as? NSDictionary {
                self.parseEntity(json: json)
            } else if !(UserDefaults.standard.value(forKey: "company_owned") as! Bool), let json = response.result.value as? NSArray {
                self.parseEntites(json: json)
            }
        }
    }
    
    func parseEntity(json: NSDictionary) {
        if let id = json["id"] as? Int64 {
            UserDefaults.standard.set(String(id), forKey: "reporter_id")
        }
        if let name = json["name"] as? String {
            self.lblName.text = name
            UserDefaults.standard.set(json["name"], forKey: "reporter_name")
        }
        if let type = json["type"] as? String {
            self.lblType.text = type
        }
        if let urlString = json["image_path"] as? String,
            let url = NSURL(string: urlString) as? URL {
            self.imvAvatar.af_setImage(withURL: url)
        }
        if let details = json["details"] as? String {
            self.lblDetails.text = details
        }
    }
    
    func parseEntites(json: NSArray) {
        //TODO: not safe, refactor
        let name = UserDefaults.standard.value(forKey: "fullname") as! String?
        lblName.text = name
        UserDefaults.standard.set(name, forKey: "reporter_name")
        lblType.text = UserDefaults.standard.value(forKey: "company_type") as! String?
        lblDetails.text = UserDefaults.standard.value(forKey: "details") as! String?
        
        let image_url = UserDefaults.standard.value(forKey: "image_path") as! String?
        if !((image_url ?? "").isEmpty) {
            imvAvatar.af_setImage(withURL: URL(string: (image_url)!)!)
        }
        
        //imvAvatar.af_setImage(withURL: URL(string: (UserDefaults.standard.value(forKey: "image_path") as! String?)!)!)
        //renew lat-lon
        entites.removeAll()
        for entity in json {
            if let e = entity as? NSDictionary {
                let newEntity = Entity()
                if let name = e["name"] as? String {
                    newEntity.name = name
                }
                if let lastreading = e["lastreading"] as? NSDictionary {
                    if let lat = lastreading["lat"] as? Double, let lon = lastreading["lng"] as? Double {
                        newEntity.lat = lat
                        newEntity.lon = lon
                    }
                    if let t = lastreading["time"] as? String {
                        let dateFormatter = DateFormatter()
                        //time = "2016-12-21T11:27:03.693740";
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
                        if let time = dateFormatter.date(from: t) {
                            newEntity.time = time
                        }
                    }
                }
                if let id = e["id"] as? Int64 {
                    newEntity.id = id
                }
                entites.append(newEntity)
            }
        }
        //renew tabbar entites
        let tbvc = self.tabBarController as! TrackTabBarController
        tbvc.entites = entites
        
        //renew markers
        print("renew markers...")
        gMapView.clear()
        let formatter = DateComponentsFormatter()
        for entity in entites {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: entity.lat, longitude: entity.lon)
            marker.title = entity.name
            print("date \(entity.time)")
            let interval = entity.time.timeIntervalSinceNow
            formatter.unitsStyle = .abbreviated
            marker.snippet = formatter.string(from: (-1)*interval)! + " ago"
            marker.map = gMapView
            marker.icon = UIImage(named: "map-marker")
        }
        // if user enable GPS then show his last location on map
        var lastLat = UserDefaults.standard.double(forKey: kLastLat)
        var lastLon = UserDefaults.standard.double(forKey: kLastLon)
        if lastLat == 0.0 && lastLon == 0.0 {
            lastLat = userLocation.coordinate.latitude
            lastLon = userLocation.coordinate.longitude
        }
        
        if lastLat != 0.0 && lastLon != 0.0 {
            gMapView.camera = GMSCameraPosition.camera(withLatitude: lastLat, longitude: lastLon, zoom: 11)
        } else { //if gps is off - show map with all entites
            var bounds = GMSCoordinateBounds()
            for entity in entites {
                if entity.lat != 0.0 && entity.lon != 0.0 {
                    bounds = bounds.includingCoordinate(CLLocationCoordinate2D(latitude: entity.lat, longitude: entity.lon))
                }
            }
            let update = GMSCameraUpdate.fit(bounds, withPadding: 100)
            gMapView.animate(with: update)
        }
    }
    
    func configUI() {
        lblName.text = ""
        lblType.text = ""
        lblDetails.text = ""
        //switch
        if let isOn = UserDefaults.standard.value(forKey: kLocationService) as? Bool {
            switchLocation.setOn(isOn, animated: false)
            UserDefaults.standard.setValue(isOn, forKey: kLocationService)
        } else {
            switchLocation.setOn(true, animated: false)
            UserDefaults.standard.setValue(true, forKey: kLocationService)
        }
        //entity
        self.imvAvatar.image = UIImage(named: ("user"))
        self.imvAvatar.layer.cornerRadius = self.imvAvatar.layer.frame.size.width/2
        self.imvAvatar.clipsToBounds = true
        messabeBarView.alpha = switchLocation.isOn ? 0.0 : 1.0
        setupEntity()
        // if user is company owned don't send location
        if let ownedCompany = UserDefaults.standard.value(forKey: kCompanyOwner) as? String,
            ownedCompany == kNotOwnedCompany,
            switchLocation.isOn {
                initLocationManager()
        } 
    }
 
    func logout() {
        self.refreshLocationTimer.invalidate()
        self.refreshMapTimer.invalidate()
        if self.locationManager != nil {
            self.locationManager.stopUpdatingLocation()
        }
    }
        
    @IBAction func switchChangeValue(_ sender: AnyObject) {
        guard self.locationManager != nil else {
            UserDefaults.standard.setValue(switchLocation.isOn, forKey: kLocationService)
            return
        }
        if locationManager != nil {
            locationManager.allowsBackgroundLocationUpdates = switchLocation.isOn
        }
        switchLocation.isOn ? self.startTimer(interval: self.refreshLocationInteval) : self.refreshLocationTimer.invalidate()
        UserDefaults.standard.setValue(switchLocation.isOn, forKey: kLocationService)
        UIView.animate(withDuration: 0.3, animations: {
            self.messabeBarView.alpha = (self.switchLocation.isOn) ? 0.0 : 1.0
        })
        if !switchLocation.isOn {
            UserDefaults.standard.setValue(0.0, forKey: kLastLon)
            UserDefaults.standard.setValue(0.0, forKey: kLastLat)
        }
        messabeBarView.alpha = switchLocation.isOn ? 0.0 : 1.0
    }
}
