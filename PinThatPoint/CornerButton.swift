//
//  CornerButton.swift
//  PinThatPoint
//
//  Created by Apple on 10/8/16.
//  Copyright © 2016 Apple. All rights reserved.
//

import UIKit

class CornerButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 3.0
        self.clipsToBounds = true
    }

}
